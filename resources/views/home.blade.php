@extends('layouts.app')

@section('title', 'SARIS')


@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">

                <div class="panel panel-default">
                    <div class="panel-heading">  <h4 >WELCOME</h4></div>
                    <div class="panel-body">
                    {{--<div class="box box-info">--}}
                            <div class="col-sm-10">
                                <h4>Hi <p style="color:#000000;"> {{ Auth::user()->name }},</p> Welcome to SARIS! please use menu in
                                the left to navigate or Click your name on top to Logout</h4></span>
                                <span><p></p></span>
                            </div>
                            <div class="clearfix"></div>

                        <!-- /.box -->
                    {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
        <!-- /.content -->
    </section>
@endsection