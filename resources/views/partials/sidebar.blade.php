
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar" >
    <!-- sidebar: style can be found in sidebar.less -->
     <section class="sidebar fixed">

        <ul class="sidebar-menu" data-widget="tree">
        @can('system_admin')
            <li class="treeview">
                <a>
                    <i class="fa fa-users"></i>
                    <span>@lang('quickadmin.user-management.title')</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                   
                    <li>
                        <a href="{{ route('admin.roles.index') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>@lang('quickadmin.roles.title')</span>
                        </a>
                    </li>
                    
                
                    <li>
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-angle-right"></i>
                            <span>@lang('quickadmin.users.title')</span>
                        </a>
                    </li>
                    
<!--       to be used in the future excluded for now         
                    <li>
                        <a href="{{ route('admin.teams.index') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>@lang('quickadmin.teams.title')</span>
                        </a>
                    </li> -->
                    
                </ul>
            </li>

            <li class="treeview">
                <a>
                    <i class="fa fa-database"></i>
                    <span>Database Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('import_data') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Import Data</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('clean db') }}">
                            <i class="fa fa-angle-right"></i>
                            <span>Clean Database</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('unknown_students') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Unknown Students</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('index_students') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Index Student</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('qrydb') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Query Database</span>
                        </a>
                    </li>
                      <li>
                        <a href="{{ route('check_conn') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Check Connections</span>
                        </a>
                    </li>
                   
                </ul>
            </li>@endcan

        @can('registrar')
         <li class="treeview">
                <a>
                    <i class="fa fa-cogs"></i>
                    <span>Policy Setup</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li>
                        <a href="{{ route('institution') }}">
                            <i class="fa fa-angle-right"></i> 
                            <span>Institution</span>
                        </a>
                    </li>
                    
                     <li>
                        <a href="{{route('study level')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Study Levels</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('sponsor') }}">
                     <i class="fa fa-angle-right"></i>
                            <span>Sponsor</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('payments') }}">
                     <i class="fa fa-angle-right"></i>
                            <span>Payment Types</span>
                        </a>
                    </li>

                     <li>
                        <a href="{{ url('attachments') }}">
                     <i class="fa fa-angle-right"></i>
                            <span>Attachments</span>
                        </a>
                    </li>
                       <li>
                        <a href="{{route('intake-categories')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Intake Category</span>
                        </a>
                    </li>

                </ul>
            </li>

             <li class="treeview">
                <a>
                    <i class="fa fa-users"></i>
                    <span>Admission Process</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                 <!--    <li>
                        <a href="{{ route('registration form') }}">
                         <i class="fa fa-angle-right"></i>
                            <span>Student List</span>
                        </a>
                    </li> -->

                    <li>
                        <a href="{{ route('Classlists') }}">
                            <i class="fa fa-angle-right"></i>
                            <span>Class Lists</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('Import Students')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Import Students</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Student Search')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Search Student</span>
                        </a>
                    </li>
                      <li>
                        <a href="{{route('Show Deleted Students')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Restore Logs</span>
                        </a>
                    </li>
                   
                </ul>
            </li>
            <li class="treeview">
                <a>
                    <i class="fa fa-home"></i>
                    <span>Accommodation</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">

                    <li>
                        {{--<a href="{{ route('hostels') }}">--}}
                            <a href="{{ url('hostels') }}">
                            <i class="fa fa-angle-right"></i>
                            <span>Hostels</span>
                        </a>
                    </li>
                </ul>
            </li>

        <!-- <li class="treeview">
                <a>
                    <i class="fa fa-check-square-o"></i>
                    <span>Evoting</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li>
                        <a href="">
                             <i class="fa fa-angle-right"></i>
                            <span>Election Posts</span>
                        </a>
                    </li>
                    
                   
                    <li>
                        <a href="">
                            <i class="fa fa-angle-right"></i>
                            <span>Set Candidates</span>
                        </a>
                    </li>
                    
                   
                    <li>
                        <a href="">
                             <i class="fa fa-angle-right"></i>
                            <span>Election Results</span>
                        </a>
                    </li>
                  
                   
                </ul>
            </li> -->
        @endcan

        @can('student')
         <li class="treeview">
                <a>
                    <i class="fa fa-info"></i>
                    <span>Basic Information</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-right"></i> 
                            <span>Personal Information</span>
                        </a>
                    </li>
                    
                   
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-right"></i>
                            <span>Bank Information</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i class="fa fa-angle-right"></i>
                            <span>Background Information</span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="treeview">
                <a>
                    <i class="fa fa-mortar-board"></i>
                    <span>Academic</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li>
                        <a href="{{route('My Courses')}}">
                            <i class="fa fa-angle-right"></i> 
                            <span>My Courses</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Results')}}">
                            <i class="fa fa-angle-right"></i> 
                            <span>Results</span>
                        </a>
                    </li>
                  

                </ul>
            </li>
            <li class="treeview">
                <a>
                    <i class="fa fa-money"></i>
                    <span>Payment</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-right"></i> 
                            <span>Request Bill</span>
                        </a>
                    </li>
             

                </ul>
            </li>
            @endcan

        @can('exam_officer')
             <li class="treeview">
                <a>
                    <i class="fa fa-gear"></i>
                    <span>Policy Setup</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li>
                        <a href="{{ route('institution') }}">
                            <i class="fa fa-angle-right"></i> 
                            <span>Institution</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('campus') }}">
                            <i class="fa fa-angle-right"></i>
                            <span>Campus</span>
                        </a>
                    </li>
                
                   
                    <li>
                        <a href="{{route('faculty')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Faculty</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Department') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Department</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Programme')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Programme</span>
                        </a>
                    </li>

                     <li>
                        <a href="{{route('intake-categories')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Intake Category</span>
                        </a>
                    </li>

                      <li>
                        <a href="{{route('Module')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Module</span>
                        </a>
                      </li>
                      <li>
                        <a href="{{route('Register Module')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Register Module</span>
                        </a>
                      </li>

                      <li>
                        <a href="{{route('Exam Category')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Exam Category</span>
                        </a>
                      </li>


                </ul>
            </li>

            <li class="treeview">
                <a>
                    <i class="fa fa-align-justify"></i>
                    <span>Adminstration</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li >
                        <a href="{{ route('Student Register') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Student Register</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Student Remarks') }}">
                            <i class="fa fa-angle-right"></i>
                            <span>Student Remarks</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Class Lists') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Class Lists</span>
                        </a>
                    </li>
                       </li>
                         <li>
                        <a href="{{ route('Update Class Lists') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Update Class Lists</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{ route('Limit Upload') }}">
                             <i class="fa fa-angle-right"></i>
                            <span>Limit Uploads</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Publish Exam')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Publish Exam</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Generate reports')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Report Generate</span>
                        </a>
                    </li>
                      <li>
                        <a href="{{route('Course Allocation')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Course Allocation</span>
                        </a>
                    </li>
                  
                   
                </ul>
            </li>
            <li class="treeview">
                <a>
                <i class="fa fa-file"></i>
                    <span>Examination</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    
                    <li>
                        <a href="{{route('Search Results')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Search</span>
                        </a>
                    </li>
                    
                   
                    <li>
                        <a href="{{route('Grade book')}}">
                            <i class="fa fa-angle-right"></i>
                            <span>Grade Book</span>
                        </a>
                    </li>
                    
                   
                    <li>
                        <a href="{{route('Course Results')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Course Result</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Semester Results')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Semester Results</span>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="{{route('NTA Semester Results')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>NTA Semester Rpt</span>
                        </a>
                    </li> -->
                      <li>
                        <a href="{{route('Annual Results')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Annual Results</span>
                        </a>
                      
                    <li>
                        <a href="{{route('Candidate Transcript')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Candidate Transcript</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Candidate Statement')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Candidate Statement</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Supplimetary Report')}}">
                             <i class="fa fa-angle-right"></i>
                            <span>Supplementary Report</span>
                        </a>
                    </li>
                   
                </ul>
            </li>
        @endcan

        
        <li class="treeview">
                <a>
                    <i class="fa fa-envelope-o"></i>
                    <span>Communication</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  
                    <li >
                        <a href="{{ route('checkmsg') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="title">Check Message</span>
                        </a>
                    </li>

                       <li >
                        <a href="{{ route('news') }}">
                             <i class="fa fa-angle-right"></i>
                            <span class="title">News and Events</span>
                        </a>
                    </li>
                    
                </ul>
            </li>
            <li class="treeview">
                <a>
                    <i class="fa fa-key"></i>
                    <span>Security</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li>
                        <a href="{{ route('auth.change_password') }}">
                             <i class="fa fa-angle-right"></i>
                            <span class="title">@lang('quickadmin.qa_change_password')</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{route('loginhistory')}}">
                            <i class="fa fa-angle-right"></i>
                            <span>Login History</span>
                        </a>
                    </li>
                    
                </ul>
            </li>
           
        </ul>
    </section>
</aside>

