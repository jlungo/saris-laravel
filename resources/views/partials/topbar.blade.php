<header class="main-header">
    <!-- Logo -->
    <a href="{!! url('/') !!}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SARIS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img href="" src="{{ url('img/ztllogo.png') }}"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ url('img/default-avatar.png') }}" class="user-image" alt="User Image">
                     @if(Auth::check())
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                     @else
                     <script type="text/javascript">
                            window.location = "{{route('login') }}";//here double curly bracket
                     </script>
                     @endif
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ url('img/default-avatar.png') }}" class="img-circle" alt="User Image">

                            @if(Auth::check())
                            <p>
                             {{ Auth::user()->name }}

                            </p>
                            @else
                            <script type="text/javascript">
                            window.location = "{{route('login') }}";
                            </script>
                            @endif

                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('profile') }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="#logout" onclick="$('#logout').submit();"
                                   class="btn btn-default btn-flat">Sign out</a>

                                <form id="logout-form" action="" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </nav>
</header>