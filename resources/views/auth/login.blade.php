@extends('layouts.auth')
@section('content')

  <div class="card card-container" >
        <img id="profile-img" class="profile-img-card" src="{{ url('img/default-avatar.png') }}" />
    <p id="profile-name" class="profile-name-card"></p>
    <form class="form-signin" method="POST" action="{{ url('login') }}">
        {{ csrf_field() }}
        <span id="reauth-email" class="reauth-email"></span>
        <input type="text" id="inputUsername" class="form-control" placeholder="Username" name="username" required autofocus>
          @error('username')
              <span class="invalid-feedback" role="alert">
                  <strong ><font color="red">{{ $message }}</font></strong>
              </span>
          @enderror
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
         @error('password')
            <span class="invalid-feedback" role="alert">
                <strong><font color="red">{{ $message }}</font></strong>
            </span>
        @enderror
        <div id="remember" class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
    </form><!-- /form -->
    <a href="{{ route('auth.password.reset') }}" class="forgot-password">
        Forgot the password?
    </a>
</div><!-- /card-container -->
@endsection