@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2 " >
            <div class="panel panel-default" style="width: 550; margin-top: 200; margin-left: 300;">
                <div class="panel-heading">Reset password</div>
                <div class="panel-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were problems with input:
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal"
                          role="form"
                          method="POST"
                          action="{{ url('password/email') }}">
                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">

                        <div class="form-group">
                        

                            <div class="col-md-6">
                                <input type="email"
                                       class="form-control"
                                       name="email"
                                       placeholder="Enter Email to reset your password" 
                                       value="{{ old('email') }}">
                            </div>
                        </div>

                       <div class="container-login100-form-btn m-t-17">
                        <button class="login100-form-btn" type="submit">
                            Reset password
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
