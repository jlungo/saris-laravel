
@extends('layouts.app')

<!-- Bootstrap core CSS -->
@section('content')
<div class="container-fluid" style="margin-right: 20px; margin-left: 20px;">   
<div class="col-sm">
         
  </div>
  <div class="box" style="padding:20px" >
      
  <div class="ibox float-e-margins">
  <div class="ibox-title">
      <h4>Course Record Sheet Examination Result</h4>
  </div>
  <div class="ibox-content">

      <form action="{{route('Download Course')}}" class="form-horizontal ng-pristine ng-valid" method="get" accept-charset="utf-8">
      {{ csrf_field() }}

      <div class="form-group"><label class="col-lg-3 control-label">Academic Year : </label>
      <div class="col-lg-8">
        <select class="form-control" name="year" >
          <option value="">--select Academic Year--</option>
          @foreach($Ayear as $yr)
          <option value="{{$yr->title}}"> {{$yr->title}} </option>
          @endforeach     
        </select>
      </div>
      </div>


        <div class="form-group"><label class="col-lg-3 control-label">Course Code : </label>

        <div class="col-lg-8">
        <select class="form-control" name="course" >
            <option value="">--select coursecode--</option>
            @foreach($module as $course)
            <option value="{{$course->coursecode}}"> {{$course->coursecode}} </option>
            @endforeach    
        </select>
        </div>
        </div>

      <div class="box-footer text-center">
                <div>
                <a href="{{route('Search Results')}}" class="btn btn-default btn-rounded mb-4" 
                  style="color: #000000;">
                  <b>BACK</b></a>

                  <button type="submit" class="btn btn-default btn-rounded mb-4"  
                  style="color: #008000;margin-left: 50px; ">
                  <b>PRINT</b></button>
               
                </div >
            </div>
      </form>   
     </div>
   </div>
</div>
</div>

@endsection