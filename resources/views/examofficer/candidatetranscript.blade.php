
@extends('layouts.app')

<!-- Bootstrap core CSS -->
@section('content')
<div class="container-fluid" style="margin-right: 20px; margin-left: 20px;">   
<div class="col-sm">
         
  </div>
  <div class="box" style="padding:20px" >
      
  <div class="ibox float-e-margins">
  <div class="ibox-title">
      <h4>Academic Transcript</h4>
  </div>
  <div class="ibox-content">
      <form action="{{route('Download PDF')}}" class="form-horizontal ng-pristine ng-valid" method="get" accept-charset="utf-8">
      {{ csrf_field() }}
      <div class="form-group"><label class="col-lg-3 control-label">Award : </label>

        <div class="col-lg-8">
            <select class="form-control" name="nta" id="institution_id">
                    <option value="">--select award--</option>
                    @foreach($award as $aw)
                    <option value=" {{$aw->levelname}}"> {{$aw->levelname}} </option>        
                    @endforeach
            </select>
        </div>
        </div>

        <div class="form-group"><label class="col-lg-3 control-label">Registration number : </label>

        <div class="col-lg-8">
        <input class="form-control" name="regno" ></input>
        </div>
        </div>


        <div class="form-group"><label class="col-lg-3 control-label">Confirmed: </label>

        <div class="col-lg-8">
        <input type="checkbox"   name="confirmed" > <b>Yes</b></input>
        </div>
        </div>
        @if (Session::has('success'))
                      <div class="alert alert-success">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                          <p>{{ Session::get('success') }}</p>
                      </div>
        @endif
        @if (Session::has('error'))
                      <div class="alert alert-error">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                          <p>{{ Session::get('error') }}</p>
                      </div>
        @endif
        <div class="box-footer text-center">
                <div>
                  <button type="submit" class="btn btn-default btn-rounded mb-4"  
                  style="color: #008000;margin-left: 50px; ">
                  <b>PRINT</b></button>
               
                </div >
            </div>
      </form>   
     </div>
   </div>
</div>
</div>

@endsection