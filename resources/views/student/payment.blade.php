
@extends('layouts.app')

  <!-- Bootstrap core CSS -->
@section('content')
<div class="container-fluid" style="margin-right: 20px; margin-left: 20px;"><div class="col-sm">
  
    </div>
        <div class="box" >
           <div class="box-header">
            <p style="align-content: center;"><strong>Student : </strong> {{$student->firstname}} {{$student->surname}} </p>
              <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalContactForm"  
                style="color: #008000;">
                <b>Add</b>
              </a>
            </div>
            <!-- /.box-header -->
            @if($payments->count()>0)
            <div class="box-body table-responsive">
             <table class="table table-hover">
              <thead>
                <tr>
                    <th>Payment Type</th>
                    <th>Amount</th>
                    <th>Semister</th>
                    <th>Year</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($payments as $pay)
                    <tr>
                      <td>{{$pay->payment_type->name}}</td>
                      <td>{{number_format($pay->amount)}}</td>
                      <td>{{$pay->semester}}</td>
                       <td>{{$pay->yearofstudy}}</td>
                      <td>
                        <form method="POST" action="{{ url('student-payment/'.$pay->id)}}" >
                          @csrf {{method_field('delete')}}
                          <button onclick="return confirm('This is permanent delete,proceed?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
            </div>
            @else
                  <p>
                    There is no any record!
                  </p>
            @endif
        </div>

       
      </div>


      <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <form class="modal-dialog"  role="form" method="POST" action="{{ url('student-payment') }}">
          {{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header text-center">
              <h4 class="modal-title w-100 font-weight-bold">New Payment</h4>
            </div>
         <div class="card-body" style="padding: 20px;">
          <input type="number" name="student" hidden value="{{$student->id}}">
          @if($paytypes->count()>0)
            <div class="form-group">
              <label>Payment Type</label>
              <select name="payment_type" class="form-control" required>
                <option value="">-select-</option>
                @foreach($paytypes as $pay)
                <option value="{{$pay->id}}">{{$pay->name}}</option>
                @endforeach
              </select>

            </div>
          @endif
            <div class="form-group">
                <input type="number" placeholder="Amount" class="form-control" id="amount" name="amount" value=""
                required>
            </div>
               <div class="form-group">
                <label>Semister</label>
                 <select name="semester" required class="form-control">
                  <option value="">-select</option>
                   <option>First Semester</option>
                   <option>Second Semester</option>
                 </select>
            </div>
          
     
            <div class="form-group">
              <button type="submit" class="btn btn-primary" style="width: 200px; height: 40px; margin-bottom: 10px;">SAVE</button>
            </div>
          </div>
          </div>
      </form> 
      </div>

     
@endsection