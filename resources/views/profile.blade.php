@extends('layouts.app')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <div class="panel panel-default">
                <div class="panel-heading">  <h4 >User Profile</h4></div>
                    {{--<div class="panel-body">--}}

                        <div class="box box-info">

                                <div class="box-body">
                                         <div class="col-sm-6">
                                         <div  align="center"> <img alt="User Pic" src="{{ url('img/default-avatar.png') }}" id="profile-image1" class="img-circle img-responsive">

                                    <input id="profile-image-upload" class="hidden" type="file">
                                    <div style="color:#999;" ></div>
                                    <!--Upload Image Js And Css-->

                                    </div>

                                  <br>

                                  <!-- /input-group -->
                                </div>
                                <div class="col-sm-6">
                                <h4 style="color:#00b1b1;">{{ Auth::user()->name }}</h4></span>
                                  <span><p></p></span>
                                </div>
                                <div class="clearfix"></div>
                       <hr style="margin:5px 0 5px 0;">


                    <div class="col-sm-5 col-xs-6 tital " > Name:</div><div class="col-sm-7 col-xs-6 ">{{ Auth::user()->name }}</div>
                         <div class="clearfix"></div>
                    <div class="bot-border"></div>

                    <div class="col-sm-5 col-xs-6 tital " >Login ID:</div><div class="col-sm-7"> {{ Auth::user()->name }}</div>
                      <div class="clearfix"></div>
                    <div class="bot-border"></div>

                    <div class="col-sm-5 col-xs-6 tital " >Status:</div><div class="col-sm-7"> Exam Officer</div>
                      <div class="clearfix"></div>
                    <div class="bot-border"></div>

                    <div class="col-sm-5 col-xs-6 tital " >E-Post:</div><div class="col-sm-7">{{ Auth::user()->email }}</div>

                      <div class="clearfix"></div>
                    <div class="bot-border"></div>

                    <div class="col-sm-5 col-xs-6 tital " >Last Login:</div><div class="col-sm-7">{{ Auth::user()->last_login_at }}</div>
                        <div class="clearfix"></div>
                        <div class="bot-border"></div>

                        <div class="col-sm-5 col-xs-6 tital " >IP Adress:</div><div class="col-sm-7">{{ Auth::user()->last_login_ip }}</div>
                        <!-- /.box-body -->
                  </div>
                  <!-- /.box -->

                </div>


                        {{--</div>--}}
            </div>
        </div>
   </div>
    <!-- /.content -->
</section>
@endsection