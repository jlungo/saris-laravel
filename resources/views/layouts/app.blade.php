<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('partials.head')
</head>

<body class="hold-transition skin-blue sidebar-mini" style="min-height: 100%;">

    <div class="wrapper" style="height: auto; min-height: 100%;">
    <div>
        <div>
        @include('partials.topbar')
        </div>
        <div>
        @include('partials.sidebar')
        </div>
            <div class="content-wrapper" >
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">SARIS</a></li>
                    <li class="has-children active">@php echo Route::current()->getName(); @endphp</li>
                </ul>
                <!-- END BREADCRUMB -->
                @if(isset($siteTitle))
                    <h3 class="page-title">
                        {{ $siteTitle }}
                    </h3>
                @endif
                <div  style="margin-right: 20px; margin-left: 20px;">
                @if (Session::has('success'))
                      <div class="alert alert-success">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                          <p>{{ Session::get('success') }}</p>
                      </div>
                @endif
                @if (Session::has('error'))
                            <div class="alert alert-error">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <p>{{ Session::get('error') }}</p>
                            </div>
                @endif
                </div>
                @yield('content')
                
                
            </div>
        
            @include('partials.footer')
    </div>
        
    </div>


{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">Logout</button>
{!! Form::close() !!}

@include('partials.javascripts')
</body>
</html>
