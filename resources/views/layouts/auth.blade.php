<!DOCTYPE html>
<html lang="en">

@include('partials.head')


<body class="page-header-fixed">

    <div class="container-fluid"  style="align-content: center;">
        @yield('content')
    </div>

    @include('partials.javascripts')

</body>
    <!--   Core JS Files   -->
    <script src="./js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="./js/bootstrap.min.js" type="text/javascript"></script>
    <!--  Charts Plugin -->
    <script src="./js/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="./js/bootstrap-notify.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="./js/light-bootstrap-dashboard.js?v=1.4.0"></script>
    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="./js/demo.js"></script>
</html>