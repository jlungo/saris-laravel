<?php

Auth::routes();
//Route::resource('admissionOrg', 'InstitutionController');

Route::get('/', function ()
 { return redirect('login'); });

 Route::get('/home', function ()
 { return redirect('home'); });

//STUDENT
//<academic>
//result
Route::get('/results', 'StudentController@studentResults')->name('Results');
Route::get('/my-courses', 'StudentController@studentCourses')->name('My Courses');
Route::post('student/{regno}','StudentController@personalinfo')->name('Personal_info');
Route::post('student-info/{regno}','StudentController@info')->name('Student_particulars');
Route::get('student/{id}/info','StudentController@basicinfo');

//END STUDENT

//home
Route::get('/home','HomeController@index')->name('home');
Route::get('','HomeController@index')->name('home');
//end home routes




//admin
Route::get('/admin/dashboard', 'SystemAdminController@dashboard')->name('dashboard');
Route::get('/admin/news_events', 'HomeController@newsevents')->name('news');
Route::post('/admin/event-add', 'EventController@store')->name('event.add');
Route::get('/admin/import_data', 'SystemAdminController@importdata')->name('import_data');
Route::get('/admin/cleandb', 'SystemAdminController@cleandatabase')->name('clean db');
Route::get('/admin/unknownstd', 'SystemAdminController@unknownstudents')->name('unknown_students');
Route::get('/admin/indexstd', 'SystemAdminController@indexstudents')->name('index_students');
Route::get('/admin/qrydb', 'SystemAdminController@querydb')->name('qrydb');
Route::get('/admin/con', 'SystemAdminController@connection')->name('check_conn');
Route::get('/admin/checkMessage', 'SystemAdminController@checkmessage')->name('checkmsg');
Route::get('/admin/loginhistory', 'SystemAdminController@loginhistory')->name('loginhistory');


//EXAM OFFICER
//(policy setup routes)
//institution
Route::get('/admin/institution', 'InstitutionsController@index')->name('institution');

Route::get('/admin/institution/{institution}', 'InstitutionsController@show')->name('Institution/Details');
Route::post('/admin/institution-add', 'InstitutionsController@store')->name('institution.add');
Route::post('/admin/institution/{id}', 'InstitutionsController@update')->name('institution.edit');
Route::post('/admin/institution-dlt/{id}', 'InstitutionsController@destroy')->name('inst.dlt');

//campus
Route::get('/admin/campus', 'CampusesController@index')->name('campus');
Route::get('/admin/campus/{campus}', 'CampusesController@show')->name('Campus/Details');
Route::post('/admin/campus-add', 'CampusesController@store')->name('campus.add');
Route::post('/admin/campus/{id}', 'CampusesController@update')->name('campus.edit');
Route::post('/admin/campus-dlt/{id}', 'CampusesController@destroy')->name('campus.dlt');

//faculty
Route::get('/admin/faculty', 'FacultyController@index')->name('faculty');
Route::get('/admin/faculty/{FacultyID}', 'FacultyController@show')->name('Faculty/Details');
Route::post('/admin/faculty-add', 'FacultyController@store')->name('faculty.add');
Route::post('/admin/faculty/{FacultyID}', 'FacultyController@update')->name('faculty.edit');
Route::post('/admin/faculty-dlt/{FacultyID}','FacultyController@destroy')->name('fac.dlt');

//department
Route::get('/admin/dept', 'DepartmentsController@index')->name('Department');
Route::post('/admin/dept-add', 'DepartmentsController@store')->name('dept.add');
Route::get('/admin/dept/{id}', 'DepartmentsController@show')->name('Department/Details');
Route::post('/admin/dept/{id}', 'DepartmentsController@update')->name('dept.edit');
Route::post('/admin/dept-dlt/{id}', 'DepartmentsController@destroy')->name('dept.dlt');

//programme
Route::get('/admin/programme', 'ProgrammesController@index')->name('Programme');
Route::post('/admin/prog-add', 'ProgrammesController@store')->name('prog.add');
Route::get('/admin/programme/{id}', 'ProgrammesController@show')->name('Programme/Details');
Route::post('/admin/programme/{id}', 'ProgrammesController@update')->name('prog.edit');
Route::post('/admin/programme-dlt/{id}', 'ProgrammesController@destroy')->name('prog.dlt');

//module
Route::get('/admin/module', 'ModulesController@index')->name('Module');
Route::post('/admin/module-add', 'ModulesController@store')->name('mod.add');
Route::get('/admin/module/{id}', 'ModulesController@show')->name('Module/Details');
Route::post('/admin/module', 'ModulesController@update')->name('edit module');
Route::post('/admin/module-dlt/{id}', 'ModulesController@destroy')->name('mod.dlt');

//register module
Route::get('/admin/register_module','ProgrammeCourseController@index')->name('Register Module');
Route::post('/admin/registermodule-add', 'ProgrammeCourseController@store')->name('regmod.add');
Route::post('/admin/module/{id}', 'ProgrammeCourseController@update')->name('mod.edit');

//Route::post('/admin/module-dlt/{id}','ProgrammeCourseController@destroy')->name('regmod.dlt');
//(policy setup routes)

//Administration
//student register
Route::get('/admin/studentregcourse', 'RegistrarController@studentreg')->name('Student Register');
Route::get('student/{regno}','RegistrarController@prev')->name('Preview Student');
Route::get('/admin/student_register', 'RegistrarController@register_students')->name('Register Students');



Route::get('/admin/courseclear', 'RegistrarController@studentCourseclear')->name('Course Clear');
Route::get('/admin/coursereg', 'RegistrarController@studentCoursereg')->name('Course Register');
Route::get('/admin/studentremarks', 'RegistrarController@studentremark')->name('Student Remarks');
Route::get('/admin/update_studentremarks', 'RegistrarController@updateStudentremark')->name('Update Student Remarks');

Route::get('/admin/limitupload', 'RegistrarController@limitUpload')->name('Limit Upload');
Route::get('/admin/setupload', 'RegistrarController@setdeadline')->name('Set Limit');


Route::get('/admin/publish', 'RegistrarController@publishexam')->name('Publish Exam');
Route::get('/admin/report_generate', 'ReportsController@showGenerateReport')->name('Generate reports');
Route::get('/admin/generate', 'ReportsController@generate')->name('Generate');

Route::get('/admin/course-allocation', 'RegistrarController@course_allocation')->name('Course Allocation');


Route::post('/admin/allocating', 'RegistrarController@courseallocating')->name('allocating');
Route::post('/admin/deallocate/{id}', 'RegistrarController@courseDeallocating')->name('deallocate');


Route::get('/admin/classlists', 'RegistrarController@class_list')->name('Class Lists');
Route::get('/admin/update-classlists', 'RegistrarController@updateClasslist')->name('Update Class Lists');
Route::get('/admin/update-class', 'RegistrarController@updateClass')->name('update classlist');

//(Examination)
//search results
Route::get('/admin/resultsrch', 'GradeBookController@searchResults')->name('Search Results');

//gradebook
Route::get('/admin/gradebook', 'GradeBookController@index')->name('Grade book');
Route::get('/admin/import', 'GradeBookController@importgradebook')->name('Import Results');
Route::get('/admin/importgrade', 'GradeBookController@importgradebook')->name('Import Grade');
Route::post('/admin/upload', 'GradeBookController@uploadResultsindex')->name('uploadresults');

//course results
Route::get('/admin/course_results', 'GradeBookController@showCourseResult')->name('Course Results');
Route::get('/admin/downloadcourse', 'GradeBookController@downloadCourseExcelResults')->name('Download Course');

//semester results
Route::get('/admin/semster_results', 'GradeBookController@showSemesterResult')->name('Semester Results');
Route::get('/admin/download_semester', 'GradeBookController@downloadSemesterExcelResults')->name('Download SemesterExcel');

//nta semester results
Route::get('/admin/ntasemster_results', 'GradeBookController@showNtaSemesterResult')->name('NTA Semester Results');
Route::get('/admin/download_ntasemester', 'GradeBookController@downloadSemesterExcelResults')->name('Download NTA SemesterExcel');

//annual resullts
Route::get('/admin/annual_results', 'GradeBookController@showAnnualResult')->name('Annual Results');
Route::get('/admin/download_annualresults', 'GradeBookController@downloadAnnualResults')->name('Download Annual');

//transcript
Route::get('/admin/cand_transcript', 'GradeBookController@showCandidateTranscript')->name('Candidate Transcript');
Route::get('/admin/download_pdf', 'GradeBookController@downloadtranscriptPdf')->name('Download PDF');

//statement
Route::get('/admin/cand_statement', 'GradeBookController@showCandidateStatement')->name('Candidate Statement');
Route::get('/admin/downloadst_pdf', 'GradeBookController@downloadStatementPdf')->name('Downloadst PDF');

//supplementary
Route::get('/admin/supp_rpt', 'GradeBookController@showSupplementaryReport')->name('Supplimetary Report');
Route::get('/admin/downloadst_rpt', 'GradeBookController@downloadSuppRptPdf')->name('Downloadrpt PDF');

//REGISTRAR
//policy setup
Route::get('/admin/sponsor', 'SponsorsController@index')->name('sponsor');
Route::post('/admin/sponsor-add', 'SponsorsController@store')->name('sponsor.add');
Route::get('/admin/sponsor/{id}', 'SponsorsController@show')->name('Sponsor/Details');
Route::post('/admin/sponsor/{id}', 'SponsorsController@update')->name('sponsor.edit');
Route::post('/admin/sponsor-dlt/{id}','SponsorsController@destroy')->name('sponsor.dlt');

//admission
Route::get('/admin/regform', 'RegistrationFormController@index')->name('registration form');
Route::post('/admin/student-add', 'RegistrarController@store')->name('student.add');

Route::get('/admin/classlist', 'RegistrarController@index')->name('Classlists');
Route::get('/admin/classlistview/{regno}', 'RegistrarController@show')->name('classlist view');

//import student
Route::get('/admin/imprtstd', 'RegistrarController@showImport')->name('Import Students');
Route::post('/admin/uploadstd', 'RegistrarController@import')->name('Upload Students');

//student search
Route::get('/admin/searchstdnt', 'RegistrarController@studentsearch')->name('Student Search');
Route::get('/admin/showuploadpicture/{regno}', 'RegistrarController@showuploadpicture')->name('show upload picture');
Route::post('/admin/uploadpicture', 'RegistrarController@uploadpicture')->name('upload picture');

//accommodation
Route::resource('hostels', 'HostelController');

//restore logs
Route::get('/admin/dltedstudents', 'RegistrarController@showrestoreLogs')->name('Show Deleted Students');
Route::post('/admin/delete_complted', 'RegistrarController@completedelete')->name('complete delete');
Route::post('/admin/restorestudents', 'RegistrarController@studentrestore')->name('restore deleted');

//all
Route::get('/admin/profile', 'HomeController@profile')->name('profile');



Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

Route::resource('roles', 'Admin\RolesController');
Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
Route::resource('users', 'Admin\UsersController');
Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
Route::resource('teams', 'Admin\TeamsController');
Route::post('teams_mass_destroy', ['uses' => 'Admin\TeamsController@massDestroy', 'as' => 'teams.mass_destroy']);
Route::resource('products', 'Admin\ProductsController');
Route::post('products_mass_destroy', ['uses' => 'Admin\ProductsController@massDestroy', 'as' => 'products.mass_destroy']);
Route::post('products_restore/{id}', ['uses' => 'Admin\ProductsController@restore', 'as' => 'products.restore']);
Route::delete('products_perma_del/{id}', ['uses' => 'Admin\ProductsController@perma_del', 'as' => 'products.perma_del']);

Route::get('/team-select', ['uses' => 'Auth\TeamSelectController@select', 'as' => 'team-select.select']);
Route::post('/team-select', ['uses' => 'Auth\TeamSelectController@storeSelect', 'as' => 'team-select.select']);

});


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('auth.login');
Route::post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
Route::get('admin/change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('admin/change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');



//NEXT OF KIN
Route::post('nextkin','NextOfKinController@store');
Route::delete('nextkin/{nextOfKin}','NextOfKinController@destroy');


//DEPENDANTS 
Route::post('dependants','DependantController@store');
Route::delete('dependants/{dependant}','DependantController@destroy');

//EDU - BACKGROUND
Route::post('edu-histry','EducationHistryController@store');
Route::delete('edu-histry/{id}','EducationHistryController@destroy');

//BANK INFO 
Route::post('bank-info','BankInfoController@store');
Route::delete('bank-info/{bankInfo}','BankInfoController@destroy');


//USER PHOTO
Route::post('photo-upload','UserPhotoController@store');


//STUDENT CONTACTS StudentContactController
Route::get('student-contacts/{regno}','StudentContactController@create');
Route::post('student-contacts','StudentContactController@store');

//FILTER STUDENTS
Route::post('filter-students','StudentController@filter');
Route::get('filter-students', 'RegistrarController@studentsearch')->name('Student Search');



//SPONSOR
Route::get('sponsor-info/{regno}','StudentSponsorController@index');
Route::post('sponsor-info','StudentSponsorController@store');
Route::patch('student-sponsors','StudentSponsorController@update');


//PAYMENTS
Route::resource('payments','PaymentTypeController');

//Student Payments
Route::get('student-payments/{id}','StudentPaymentController@index');
Route::post('student-payment','StudentPaymentController@store');
Route::delete('student-payment/{id}','StudentPaymentController@destroy');


//Attachment controller
Route::resource('attachments','AttachmentController');


//Student Attachment student-attachment
Route::get('student-attachment/{std}','StudentAttachmentController@index');
Route::post('student-attachments/{std}','StudentAttachmentController@store');
Route::post('update-student-attachments','StudentAttachmentController@update');
Route::delete('remove-attachment/{att}','StudentAttachmentController@destroy');



//EXAM CATEGORY
Route::get('admin/examcategory','ExamCategoryController@index')->name('Exam Category');
Route::post('categories','ExamCategoryController@store')->name('categories');
Route::get('/admin/examcategory/{cat}', 'ExamCategoryController@show')->name('examcategory/preview');
Route::patch('/admin/examcategory/{cat}', 'ExamCategoryController@update')->name('examcategory.edit');



//INTAKE CATEGORIES
Route::get('/admin/intake-category','IntakeCategoryController@index')->name('intake-categories');
Route::post('/admin/intake-add', 'IntakeCategoryController@store')->name('intake.add');
Route::get('/admin/intake/{intake}', 'IntakeCategoryController@show')->name('Intake-Category/Details');
Route::post('/admin/intake-category/{id}', 'IntakeCategoryController@update')->name('intake.edit');
Route::post('/admin/intake-category-dlt/{id}', 'IntakeCategoryController@destroy')->name('int.dlt');


//Module configuration
Route::get('admin/programe/{id}/module-configure','ModuleConfigurationController@index')->name('module configuration');
Route::post('module configure','ModuleConfigurationController@store')->name('module configure');
Route::get('/admin/prog-module/{id}/edit','ModuleConfigurationController@edit');
Route::patch('update program module','ModuleConfigurationController@update')->name('update program module');
Route::post('delete-progr-module','ModuleConfigurationController@destroy')->name('delete-progr-module');


//Credit configuration 
Route::get('admin/programe/{id}/credit-configure','CreditConfigurationController@index')->name('credit configuration');
Route::post('credit configure','CreditConfigurationController@store')->name('credit configure');
Route::get('/admin/prog-credit/{id}/edit','CreditConfigurationController@edit');
Route::patch('update program credit','CreditConfigurationController@update')->name('update program credit');
Route::post('delete-progr-credit','CreditConfigurationController@destroy')->name('delete-progr-credit');


//STudy Level
Route::get('/admin/studylevel', 'StudyLevelController@index')->name('study level');

Route::get('/admin/studylevel/{stdylevel}', 'StudyLevelController@show')->name('studylevel/Details');
Route::post('/admin/stdylevel-add', 'StudyLevelController@store')->name('stdylevel.add');
Route::post('/admin/studylevel/{id}', 'StudyLevelController@update')->name('stdylevel.edit');
Route::post('/admin/studylevel-dlt/{id}', 'StudyLevelController@destroy')->name('inst.dlt');