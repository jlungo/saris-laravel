<?php

use Illuminate\Database\Seeder;

class ClassStreamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'NTA4-Morning','group'=>''],
            ['id' => 2, 'name' => 'NTA4-Evening','group'=>''],
            ['id' => 3, 'name' => 'NTA5-Morning','group'=>''],
            ['id' => 4, 'name' => 'NTA5-Evening','group'=>''],
            ['id' => 5, 'name' => 'NTA6-Morning','group'=>''],
            ['id' => 6, 'name' => 'NTA6-Evening','group'=>''],
            ['id' => 7, 'name' => 'NTA7-Morning','group'=>''],
            ['id' => 8, 'name' => 'NTA7-Evening','group'=>''],
            ['id' => 9, 'name' => 'NTA8-Morning','group'=>''],
            ['id' => 10, 'name' => 'NTA8-Evening','group'=>''],
            ['id' => 11, 'name' => 'NTA9-Morning','group'=>''],
            ['id' => 12, 'name' => 'NTA9-Evening','group'=>''],
            ['id' => 13, 'name' => 'PGD-Morning','group'=>''],
            ['id' => 14, 'name' => 'PGD-Evening','group'=>'']

        ];

        foreach ($items as $item) {
            \App\ClassStream::create($item);
        }
    }
}
