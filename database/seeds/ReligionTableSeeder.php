<?php

use Illuminate\Database\Seeder;

class ReligionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'religion' => 'Muslim'],
            ['id' => 2, 'religion' => 'Christian'],
            ['id' => 3, 'religion' => 'Other']
         ];

        foreach ($items as $item) {
            \App\Religion::create($item);
        }
    }
}

