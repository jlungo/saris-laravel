<?php

use Illuminate\Database\Seeder;
use App\PaymentType;

class PaymentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $spn = new PaymentType;
          $spn->name = 'Tuition Fee';
          $spn->descriptions = 'descriptions here';
          $spn->save();

          $spn1 = new PaymentType;
          $spn1->name = 'Transcript Fee';
          $spn1->descriptions = 'descriptions';
          $spn1->save();

          $spn2 = new PaymentType;
          $spn2->name = 'Provisional Results Fee';
          $spn2->descriptions = 'descriptions';
          $spn2->save();
    }
}

