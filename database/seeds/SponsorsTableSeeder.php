<?php

use Illuminate\Database\Seeder;
use App\Sponsor;

class SponsorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         

          $spn = new Sponsor;
          $spn->sponsorname = 'HESLB';
          $spn->address = 'DODOMA';
          $spn->comment = 'Payable by 85%';
          $spn->save();

          $spn1 = new Sponsor;
          $spn1->sponsorname = 'WHO';
          $spn1->address = 'DAR ES SALAAM';
          $spn1->comment = 'Payable by 90%';
          $spn1->save();

          $spn2 = new Sponsor;
          $spn2->sponsorname = 'Steve Jobs Fund';
          $spn2->address = 'MWANZA';
          $spn2->comment = 'Payable by 100%';
          $spn2->save();


    }
}
