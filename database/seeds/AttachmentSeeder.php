<?php

use Illuminate\Database\Seeder;
use App\Attachment;

class AttachmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new Attachment;
        $new->name = 'Birth Certificate';
        $new->description = 'A document showing details about birth';
        $new->save();

    }
}
