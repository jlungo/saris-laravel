<?php

use Illuminate\Database\Seeder;

class ExamCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'description' => 'Hoomework one (1)'],
            ['id' => 2, 'description' => 'Hoomework Two (2)'],
            ['id' => 3, 'description' => 'Quiz One (1)'],
            ['id' => 4, 'description' => 'Coursework'],
            ['id' => 5, 'description' => 'Semester Examination'],
            ['id' => 6, 'description' => 'Group Assignments'],
            ['id' => 7, 'description' => 'Supplementary Exam'],
            ['id' => 8, 'description' => 'Industrial Practical Training'],
            ['id' => 9, 'description' => 'Classroom Test One (1)'],
            ['id' => 10,'description' => 'Classroom Test Two (2)'],
            ['id' => 11,'description' => 'Special Examination'],
            ['id' => 12,'description' => 'Research/Project']
            ];

            foreach ($items as $item) {
                \App\ExamCategory::create($item);
            }

    }
}
