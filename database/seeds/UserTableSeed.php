<?php

use Illuminate\Database\Seeder;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'name' => 'System Admin',
                'username' => 'admin',
                'email' => 'admin@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 1,
                'remember_token' => '',
                ],
            [
                'id' => 2,
                'name' => 'Exam Officer',
                'username' => 'examofficer',
                'email' => 'examofficer@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 4,
                'remember_token' => '',
            ],

            [
                'id' => 3,
                'name' => 'Registrar Officer',
                'username' => 'registrar',
                'email' => 'registrar@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 5,
                'remember_token' => '',
            ],


            [
                'id' => 4,
                'name' => 'Timetable Module',
                'username' => 'timetable',
                'email' => 'timetable@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 6,
                'remember_token' => '',
            ],


            [
                'id' => 5,
                'name' => 'OAS Module',
                'username' => 'oas',
                'email' => 'oas@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 10,
                'remember_token' => '',
            ],


            [
                'id' => 6,
                'name' => 'Billing Module',
                'username' => 'billing',
                'email' => 'billing@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 8,
                'remember_token' => '',
            ],

            [
                'id' => 7,
                'name' => 'Accommodation Module',
                'username' => 'accom',
                'email' => 'accom@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 9,
                'remember_token' => '',
            ],

            [
                'id' => 8,
                'name' => 'Student Module',
                'username' => 'student',
                'email' => 'student@admin.com',
                'password' => password_hash("zalongwa",PASSWORD_DEFAULT),
                'role_id' => 7,
                'remember_token' => '',
            ],
        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
