<?php

use Illuminate\Database\Seeder;

class MaritalStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

      $marital_statuses = array (
       array('status' => 'Single'),
       array('status' => 'Married'),
       array('status' => 'Widow'),
       array('status' => 'Engaged'),
      );

      foreach ($marital_statuses as $marital_status){
          \App\MaritalStatus::create($marital_status);
      }
    }
}
