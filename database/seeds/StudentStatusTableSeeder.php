<?php

use Illuminate\Database\Seeder;

class StudentStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'status' => 'Absconded'],
            ['id' => 2, 'status' => 'Temp.Registered'],
            ['id' => 3, 'status' => 'Registered'],
            ['id' => 4, 'status' => 'Discountinued'],
            ['id' => 5, 'status' => 'Graduate'],
            ['id' => 6, 'status' => 'Died'],
            ['id' => 7, 'status' => 'Postponed'],
            ['id' => 8, 'status' => 'Not Registered'],
            ['id' => 9, 'status' => 'Resume'],
            ['id' => 10, 'status' => 'Retake']
         ];


        foreach ($items as $item) {
            \App\StudentStatus::create($item);
        }
    }
}
