<?php

use Illuminate\Database\Seeder;

class MannerofEntryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'mannerofentry' => 'Direct'],
            ['id' => 2, 'mannerofentry' => 'Equivalent'],
            ['id' => 3, 'mannerofentry' => 'Mature Entry'],
            ['id' => 4, 'mannerofentry' => 'Pre-Entry'],
            ['id' => 5, 'mannerofentry' => 'Transfered']
         ];


        foreach ($items as $item) {
            \App\MannerOfEntry::create($item);
        }

    }
}
