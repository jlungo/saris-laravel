<?php

use Illuminate\Database\Seeder;

class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'semester' => 'semester I'],
            ['id' => 2, 'semester' => 'semester II']
        ];
        foreach ($items as $item) {
            \App\Semester::create($item);
        }
    }
}
