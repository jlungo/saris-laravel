<?php

use Illuminate\Database\Seeder;

class StudentClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'class' => 'First Year'],
            ['id' => 2, 'class' => 'Second Year'],
            ['id' => 3, 'class' => 'Third Year'],
            ['id' => 4, 'class' => 'Fourth Year'],
            ['id' => 5, 'class' => 'Fifth Year']
        ];
        foreach ($items as $item) {
            \App\StudentClass::create($item);
        }
    }
}
