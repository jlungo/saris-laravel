<?php

use Illuminate\Database\Seeder;

class StudyLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           $items = [
    	[
        'levelname' => 'NTA Level 4'
        ],
           [
        'levelname' => 'NTA Level 5'
        ],
        [
        'levelname' => 'NTA Level 6'
        ],
        [
        'levelname' => 'NTA Level 7'
        ],
        [
        'levelname' => 'NTA Level 8'
        ],
			[
        'levelname' => 'NTA Level 9'
        ]
        ];

        foreach ($items as $item) {
            \App\StudyLevel::create($item);
        }
    }
    
}
