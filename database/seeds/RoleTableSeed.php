<?php

use Illuminate\Database\Seeder;

class RoleTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
           ['id' => 1, 'title' => 'Administrator (can create other users)',],
           ['id' => 2, 'title' => 'Simple user',],
           ['id' => 3, 'title' => 'Team Admin',],
           ['id' => 4, 'title' => 'Exam officer',],
           ['id' => 5, 'title' => 'Registrar',],
           ['id' => 6, 'title' => 'Timetable',],
           ['id' => 7, 'title' => 'Student',],
            ['id' => 8, 'title' => 'Billing',],
            ['id' => 9, 'title' => 'Accommodation',],
            ['id' => 10, 'title' => 'OAS',],

        ];

        foreach ($items as $item) {
            \App\Role::create($item);
        }
    }
}
