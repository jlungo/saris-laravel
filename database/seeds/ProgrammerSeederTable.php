<?php

use Illuminate\Database\Seeder;
use App\ProgrammeCourse;

class ProgrammerSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $new = new ProgrammeCourse;
        $new->programme = 'Satellite Communication';
        $new->semester = 1;
    }
}
