<?php

use Illuminate\Database\Seeder;

class DisabilityCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'disabilitycategory' => 'Blind','disabilitycode'=>1],
            ['id' => 2, 'disabilitycategory' => 'Hearing disabled','disabilitycode'=>2],
            ['id' => 3,  'disabilitycategory' => 'Leg disabled','disabilitycode'=>3]
            ];

            foreach ($items as $item) {
                \App\DisabilityCategory::create($item);
            }
            
    }
}
