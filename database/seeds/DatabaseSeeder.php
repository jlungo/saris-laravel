<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SemesterTableSeeder::class);
        $this->call(ClassStreamTableSeeder::class);
        $this->call(DisabilityCategoryTableSeeder::class);
        $this->call(MannerofEntryTableSeeder::class);
        $this->call(MaritalStatusTableSeeder::class);
        $this->call(ReligionTableSeeder::class);
        $this->call(StudentStatusTableSeeder::class);
        $this->call(YearofStudyTableSeeder::class);
        $this->call(RoleTableSeed::class);
        $this->call(UserTableSeed::class);
        $this->call(StudyLevelTableSeeder::class);
        $this->call(SponsorsTableSeeder::class);
        $this->call(PaymentTypesSeeder::class);
        $this->call(AttachmentSeeder::class);

    }
}
