<?php

use Illuminate\Database\Seeder;

class YearofStudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items=[
        ['id' => 1, 'title' => '2016/2017'],
        ['id' => 2, 'title' => '2017/2018'],
        ['id' => 3, 'title' => '2018/2019'],
        ['id' => 4, 'title' => '2019/2020']
    ];


    foreach ($items as $item) {
        \App\YearOfStudy::create($item);
    }
    }
}
