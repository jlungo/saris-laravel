<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationHistriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_histries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('regno');
            $table->foreign('regno')->references('regno')->on('students');
            $table->string('level');
            $table->string('index_no');
            $table->string('start_year');
            $table->string('end_year');
            $table->string('grade');
            $table->string('institution_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_histries');
    }
}
