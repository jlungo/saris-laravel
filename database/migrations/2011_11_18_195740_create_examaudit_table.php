<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamauditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examaudit', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Ayear')->nullable();
            $table->string('semester')->nullable();
            $table->string('marker')->nullable();
            $table->string('coursecode')->nullable();
            $table->string('examCAtegory')->nullable();
            $table->string('examdate')->nullable();
            $table->string('examsitting')->nullable();
            $table->string('recorder')->nullable();
            $table->string('recorddate')->nullable();
            $table->string('regno');
            $table->string('examno')->nullable();
            $table->string('checked')->nullable();
            $table->string('examscore')->nullable();
            $table->string('exaxmscoreAfter')->nullable();
            $table->string('actiontime')->nullable();
            $table->string('action_user')->nullable();
            $table->string('action_value')->nullable();
            $table->string('status')->nullable();
            $table->string('count')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examaudit');
    }
}
