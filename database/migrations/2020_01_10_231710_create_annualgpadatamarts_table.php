<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualgpadatamartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annualgpadatamarts', function (Blueprint $table) {
            $table->string('regno');
            $table->string('Ayear');
            $table->string('class');
            $table->string('credits');
            $table->string('points');
            $table->string('gpa');
            $table->string('remarks');
            $table->foreign('regno')->references('regno')->on('students')->onDelete('cascade');
            $table->primary(['regno','class','Ayear']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annualgpadatamarts');
    }
}
