<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammecourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmecourse', function (Blueprint $table) {
            $table->string('programme');
            $table->string('semester');
            $table->string('class');
            $table->string('coursecode');
            $table->string('status');
            $table->string('intakeyear')->nullable();
            $table->primary(['programme','semester','class','coursecode']);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programmecourse');
    }
}
