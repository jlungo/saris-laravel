<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesCourseAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectures_course_allocation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lecturername');
            $table->string('position')->nullable();
            $table->string('Ayear');
            $table->string('semester');
            $table->string('coursecode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectures_course_allocation');
    }
}
