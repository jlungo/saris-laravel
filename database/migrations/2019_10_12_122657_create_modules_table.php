<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('moduleid');
            $table->string('modulename');
            $table->string('coursecode');
            $table->integer('capacity')->nullable();
            $table->integer('credits');
            $table->integer('studylevel_id')->unsigned();
            $table->foreign('studylevel_id')->references('id')->on('studylevels')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
