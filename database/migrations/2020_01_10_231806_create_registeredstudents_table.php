<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisteredstudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registeredstudents', function (Blueprint $table) {

            $table->string('regno');
            $table->string('Ayear');
            $table->string('class');
            $table->string('semester');
            $table->string('status');
            $table->foreign('regno')->references('regno')->on('students')->onDelete('cascade');
            $table->primary(['regno','class','Ayear','semester']);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registeredstudents');

    }
}
