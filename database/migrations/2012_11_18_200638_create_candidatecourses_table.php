<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatecoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatecourses', function (Blueprint $table) {
            $table->string('Ayear');
            $table->string('semester');
            $table->string('coursecode');
            $table->string('status')->nullable();
            $table->string('regno');
            $table->foreign('regno')->references('regno')->on('students')->onDelete('cascade');
            $table->primary(['Ayear', 'semester','coursecode','regno']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidatecourses');
    }
}
