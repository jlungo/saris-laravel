<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentrestoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentrestore', function (Blueprint $table) {
            $table->string('regno')->unique();
            $table->string('action');
            $table->string('date');
            $table->string('user');
            $table->string('surname',30);
            $table->string('firstname',30);
            $table->string('middlename',30)->nullable();
            $table->String('IntakeValue')->nullable();
            $table->string('sex')->nullable();
            $table->string('dbirth')->nullable();
            $table->string('mannerofentry')->nullable();
            $table->string('maritalstatus')->nullable();
            $table->string('campus')->nullable();
            $table->string('programmeofstudy')->nullable();
            $table->string('subject')->nullable();
            $table->string('faculty')->nullable();
            $table->string('department')->nullable();
            $table->string('sponsor')->nullable();
            $table->string('gradyear')->nullable();
            $table->string('entryyear')->nullable();
            $table->string('status')->nullable();
            $table->string('yearofstudy')->nullable();
            $table->string('address')->nullable();
            $table->string('comment')->nullable();
            $table->string('photo')->nullable();
            $table->string('idprocess')->nullable();
            $table->string('nationality')->nullable();
            $table->string('region')->nullable();
            $table->string('district')->nullable();
            $table->string('country')->nullable();
            $table->string('parentoccupation')->nullable();
            $table->string('received')->nullable();
           
            $table->string('display')->nullable();
            $table->string('denomination')->nullable();
            $table->string('religion')->nullable();
            $table->string('disability')->nullable();
            $table->string('formfour')->nullable();
            $table->string('formsix')->nullable();
            $table->string('diploma')->nullable();
            $table->string('father')->nullable();
            $table->string('fatherjob')->nullable();
            $table->string('fatherphone')->nullable();
            $table->string('mother')->nullable();
            $table->string('motherjob')->nullable();
            $table->string('motherphone')->nullable();
            $table->string('kin')->nullable();
            $table->string('kinjob')->nullable();
            $table->string('kinphone')->nullable();
            $table->string('kinaddress')->nullable();
            
            $table->string('disabilitycategory')->nullable();
            $table->string('f4year')->nullable();
            $table->string('f6year')->nullable();
            $table->string('s7year')->nullable();
            $table->string('accountnumber')->nullable();
            $table->string('bankbranchname')->nullable();
            $table->string('bankname')->nullable();
            $table->string('form4no')->nullable();
            $table->string('form4name')->nullable();
            $table->string('form6name')->nullable();
            $table->string('form6no')->nullable();
            $table->string('std7no')->nullable();
            $table->string('std7name')->nullable();
            $table->string('paddress')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('currentaddress')->nullable();
            $table->string('studylevel')->nullable();
            $table->string('class')->nullable();
            $table->string('lastaddress')->nullable();
            $table->timestamps();
            $table->primary(['regno']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studentrestore');
    }
}
