<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('course_results', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('Ayear');
        //     $table->integer('semester')->unsigned();
        //     $table->foreign('semester')->references('id')->on('semesters')->onDelete('cascade');
        //     $table->string('coursecode');
        //     $table->string('coursecode_credit');
        //     $table->string('totalscore')->default(0);;
        //     $table->string('grade')->default(0);
        //     $table->string('remarks')->nullable();
        //     $table->string('points')->default(0);
        //     $table->string('gpa')->default(0);
        //    $table->primary(['coursecode']);
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_results');
    }
}
