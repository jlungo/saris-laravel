<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments',
            function (Blueprint $table) {
            $table->increments('DeptID');
            $table->integer('FacultyID')->unsigned();
            $table->string('DeptName');
            $table->string('DeptPhysAdd')->nullable();
            $table->string('DeptAddress')->nullable();
            $table->string('DeptTel')->nullable();
            $table->string('DeptEmail')->nullable();
            $table->string('DeptHead')->nullable();
            $table->foreign('FacultyID')->references('FacultyID')->on('faculties')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
