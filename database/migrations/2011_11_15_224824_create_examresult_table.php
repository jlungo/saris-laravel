<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamresultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examresults', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Ayear');
            $table->string('semester');
            $table->string('marker')->nullable();
            $table->string('coursecode');
            $table->string('coursecode_credit');
            $table->string('examcategory');
            $table->string('examdate')->nullable();
            $table->string('examsitting')->nullable();
            $table->string('recorder')->nullable();
            $table->string('recorddate')->nullable();
            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->string('examno')->nullable();
            $table->string('checked')->nullable();
            $table->string('examscore','0')->nullable();
            $table->string('status')->nullable();
            $table->string('count')->nullable();
            $table->string('comment')->nullable();
            // $table->primary(['coursecode','examcategory','semester']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examresult');
    }
}
