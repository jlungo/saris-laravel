<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_configurations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('module_code');
            $table->string('year_of_study');
            $table->string('category');
            $table->integer('semester_id')->unsigned();
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('cascade');
            $table->integer('programme_id')->unsigned();
            $table->foreign('programme_id')->references('ProgrammeID')->on('programmes')->onDelete('cascade');
            $table->string('credit');
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_configurations');
    }
}
