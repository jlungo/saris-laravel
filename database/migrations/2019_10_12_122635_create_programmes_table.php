<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmes', function (Blueprint $table) {
            $table->increments('ProgrammeID');
            $table->integer('ProgrammeCode',0)->nullable();
            $table->string('ProgrammeName');
            $table->string('Ntalevel');
            $table->integer('DeptID')->unsigned();
            $table->foreign('DeptID')->references('DeptID')->on('departments')->onDelete('cascade');
            $table->longText('Title')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programmes');
    }
}
