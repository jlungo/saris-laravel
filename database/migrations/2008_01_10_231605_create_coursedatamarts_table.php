<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursedatamartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coursedatamarts', function (Blueprint $table) {
            $table->string('name');
            $table->string('regno');
            $table->string('Ayear');
            $table->string('class');
            $table->string('semester');
            $table->string('coursecode');
            $table->string('coursework');
            $table->string('finalexam');
            $table->string('total');
            $table->string('grade');
            $table->string('remarks');
            $table->primary(['regno','class','Ayear','coursecode']);
            $table->foreign('regno')->references('regno')->on('students')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coursedatamarts');
    }
}
