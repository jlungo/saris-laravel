<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faculties', function (Blueprint $table) {
            $table->increments('FacultyID');
            $table->integer('CampusID',0)->unsigned();
            $table->foreign('CampusID')->references('id')->on('campuses')->onDelete('cascade'); 
            $table->string('FacultyName');
            $table->string('Address')->nullable();
            $table->string('Email')->nullable();
            $table->string('Tel')->nullable();
            $table->string('Location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faculties');
    }
}
