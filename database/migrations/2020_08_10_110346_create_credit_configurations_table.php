<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_configurations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('year_of_study');
            $table->string('academic_year');
            $table->integer('semester_id')->unsigned();
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('cascade');
            $table->integer('programme_id')->unsigned();
            $table->foreign('programme_id')->references('ProgrammeID')->on('programmes')->onDelete('cascade');
            $table->string('credit');
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_configurations');
    }
}
