<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id'); //ok
            $table->string('regno')->unique()->onDelete('cascade'); //ok
            $table->string('surname',30); //ok
            $table->string('firstname',30); //ok
            $table->string('middlename',30)->nullable(); //ok
            $table->String('IntakeValue')->nullable();
            $table->string('sex');  //ok
            $table->string('dbirth');  //ok should be null as defined from excelsheet
            $table->string('mannerofentry')->nullable();
            $table->string('maritalstatus');  //ok
            $table->string('campus')->nullable();  //mandate this
            $table->string('programmeofstudy')->nullable();
            $table->string('subject')->nullable();
            $table->string('faculty')->nullable();
            $table->string('department')->nullable();
            $table->string('entryyear')->nullable();
            $table->string('countrybirth')->nullable();
            $table->string('regionbirth')->nullable();
            $table->string('districtbirth')->nullable();
            $table->string('gradyear')->nullable();
            $table->string('status')->nullable();
            $table->string('yearofstudy')->nullable();
            $table->string('comment')->nullable();
            $table->string('idprocess')->nullable();
            $table->string('nationality')->nullable();
            $table->string('received')->nullable();
            $table->string('user')->nullable();
            $table->string('display')->nullable();
            $table->string('denomination')->nullable();
            $table->string('religion')->nullable();
            $table->string('s7year')->nullable();
            $table->string('std7no')->nullable();
            $table->string('form4year')->nullable();
            $table->string('form4no')->nullable();
            $table->string('studylevel')->nullable();
            $table->string('class')->nullable();
            $table->timestamps();


            // add course selected BSTS
            // add Level, Diploma or degreee  - these will help to generater regno
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
