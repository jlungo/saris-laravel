<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_sponsors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('regno');
            $table->foreign('regno')->references('regno')->on('students');
            $table->string('address')->nullable();
            $table->string('sponsorId')->nullable();
            $table->string('sponsor_type'); //institute or private
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('occupation')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_sponsors');
    }
}
