<?php

namespace App\Imports;

use App\Student;
use App\User;
//use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Validation\Rule;
use DB;

class StudentsImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    // public function model(array $row)
    // {
    //     return new Student([    
    //         'Sno'=>$row[0],        
    //         'regno'     => $row[1],
    //         'firstname'    => $row[2], 
    //         'surname'    => $row[3], 
    //     ]);
    // }

    public function collection(Collection $rows)
    {

       // dd($intakeyear); 

        // foreach ($rows as $row) 
        // {
        //     Student::create([
        //         'regno'=> $row[1],
        //         'firstname'=> $row[2], 
        //         'surname' => $row[3], 
        //     ]);
        // }

        $data = [];




        foreach ($rows as $row) 
             if ($row->filter()->isNotEmpty()) {
                    $data[] = array(
                            'regno'  => $row[1],
                            'firstname' => $row[2],
                            'middlename' => $row[3],
                            'surname' => $row[4],
                            'sex' => $row[5],
                            'sponsor' => $row[6],
                            'dbirth' => $row[7],
                            'formfour' => $row[8],
                            'f4year' => $row[9],
                            'formsix' => $row[10],
                            'f6year' => $row[11],
                            'nationality' => $row[13],
                        );

             }


         //    dd($data);

            
        if(!empty($data)){
            foreach ($data as $key => $value) {
                $regnumber = $value['regno'];                
                 if ($regnumber =='') {
                     continue;
                 }else{
                    $arrStudent[] = [
                        'regno' => $regnumber,
                        'firstname'=> $value['firstname'],
                        'middlename' => $value['middlename'],
                        'surname'=>$value['surname'],
                        // 'IntakeValue' => $intakeyear,
                        'sex' => $value['sex'],
                        // 'email'=> $value['email'],
                        // 'campus'=>$campus,
                        // 'programmeofstudy'=>$programme
                    ];
                    $arrUser[] = [
                        'username' => $regnumber,
                        'name' => $value['surname'].' '.$value['firstname'].' '.$value['middlename'],
                        'email'=> $value['email'],
                        'password' => password_hash($value['surname'],PASSWORD_DEFAULT),
                        'role_id' => 7,
                        
                    ];

                 }
            
            }if(!empty( $arrStudent)){
                if($update=='yes'){
                    try{
                    foreach ($data as $key => $value) {
                        $regnumber =$value->regno;
                        if ($regnumber =='') { continue;}
                           
                        $wherevalues1 = [
                            ['regno', $value->regno]
                        ];
                        $wherevalues = [
                            ['username', $value->regno]
                        ];
                    Student::where($wherevalues1 )
                    ->update([
                        'regno' => $regnumber,
                        'firstname'=> $value['firstname'],
                        'middlename' => $value['middlename'],
                        'surname'=>$value['surname'],
                        // 'IntakeValue' => $intakeyear,
                        'sex' => $value['sex'],
                        // 'email'=> $value['email'],
                        // 'campus'=>$campus,
                        // 'programmeofstudy'=>$programme
                        ]);

                    User::where($wherevalues)
                    ->update([
                        'username' => $regnumber,
                        'name' => $value['surname'].' '.$value['firstname'].' '.$value['middlename'],
                        'email'=> $value['email'],
                        'password' => password_hash($value['surname'],PASSWORD_DEFAULT),
                        'role_id' => 7,]); 

                    }
                        return back()->with('success', 'Update Students Successfully.');
                    }catch(Exception $e){
                        return back()->with('error','Error updating Students');
                    }
    
                }else{
                    try{
                    Student::create($arrStudent);
                    User::create($arrUser);
                    return back()->with('success', 'Insert Record successfully.');
                    }catch(Exception $e){
                        return back()->with('error','Some Students already exists check overwrite to update');
                    } 
                }    
                
            }else{
                return back()->with('error', 'No Students to be uploaded!');
            }
    
        }


    }

           


}
