<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YearOfStudy extends Model
{
    protected $table = 'yearofstudy';
    protected $fillable = ['title'];
}
