<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //protected $primaryKey = 'regno';
    protected $table = 'students';
    protected $fillable = [
            'regno',
            'surname',
            'firstname',
            'middlename',
            'IntakeValue',
            'sex',
            'dbirth',
            'mannerofentry',
            'maritalstatus',
            'campus',
            'programmeofstudy',
            'subject',
            'faculty',
            'department',
            'gradyear',
            'entryyear',
            'status',
            'yearofstudy',
            'address',
            'comment',
            'idprocess',
            'nationality',
            'region',
            'district',
            'country',
            'received',
            'user',
            'display',
            'denomination',
            'religion',
            'disability',
            'diploma',
            'disabilitycategory',
            'paddress',
            'email',
            'phone',
            'currentaddress',
            'studylevel',
            'class',
            'countrybirth',
            'regionbirth',
            'districtbirth'
    ];  

    public function nextof()
    {
      return $this->hasMany(NextOfKin::class,'regno');
    }

    public function dependant()
    {
      return $this->hasMany(Dependant::class,'regno');
    }

    public function contact()
    {
        return $this->hasOne(StudentContact::class,'regno');
    }

    public function attach()
    {
        return $this->hasMany(StudentAttachment::class,'applicant_id');
    }
   
}


