<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisabilityCategory extends Model
{
    protected $table = 'disabilitycategory';
    protected $fillable = ['disabilitycategory','disabilitycode'];
}
