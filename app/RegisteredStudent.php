<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredStudent extends Model
{
    protected $table ='registeredstudents';
    protected $fillable = [
        'regno',
        'Ayear',
        'class',
        'semester',
        'status',
    ];
}
