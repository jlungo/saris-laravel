<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class ModuleConfiguration extends Model
{
    protected $fillable = ['programme_id','module_code','year_of_study','credit','semester_id','category'];

    public function semester()
    {
    	return $this->belongsTo('App\Semester');
    }

    public function year()
    {
    	return $this->belongsTo('App\YearOfStudy');
    }
}


