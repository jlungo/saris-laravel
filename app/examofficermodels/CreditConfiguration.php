<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class CreditConfiguration extends Model
{
    protected $fillable = ['programme_id','academic_year','year_of_study','credit','semester_id'];

    public function semester()
    {
    	return $this->belongsTo('App\Semester');
    }

     public function program()
    {
    	return $this->belongsTo(Programme::class);
    }

    public function year()
    {
    	return $this->belongsTo('App\YearOfStudy');
    }
}
