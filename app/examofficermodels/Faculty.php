<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
	protected $primaryKey = 'FacultyID';
    protected $fillable = ['FacultyName','CampusID', 'Address', 'Tel','Location', 'Email'];
    protected $hidden = [];



   public function campus(){

      return $this->belongsTo('App\examofficermodels\Campus');

    }


}
