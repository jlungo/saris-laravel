<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $fillable = ['Name', 'Address', 'city','tel','fax', 'website', 'email'];
    protected $hidden = [];




}
