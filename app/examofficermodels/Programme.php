<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
	protected $primaryKey=  'ProgrammeID';
    protected $fillable = ['ProgrammeCode', 'ProgrammeName', 'Ntalevel', 'Title','DeptID'];
 


   public function department(){

      return $this->belongsTo('App\examofficermodels\Department');

    }

   public function modules(){

      return $this->hasMany('App\examofficermodels\Module');

    }

}
