<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	protected $primaryKey =  'moduleid';
    protected $fillable = ['modulename','coursecode','credits','capacity','studylevel_id'];


    public function programme(){

      return $this->belongsTo('App\examofficermodels\Programme');
      
    }

    public function studylevel(){

      return $this->belongsTo('App\StudyLevel');
      
    }

}
