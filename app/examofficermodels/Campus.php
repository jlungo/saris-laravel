<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
   protected $fillable = [
   	'campus',
    'address',
     'tel', 
     'institution_id',
     'email'
 ];

public function institutions(){
	
	return $this->belongsTo('App\examofficermodels\Institution');
	
}


}

