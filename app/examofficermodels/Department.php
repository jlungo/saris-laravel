<?php

namespace App\examofficermodels;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $primaryKey=  'DeptID';
    
	protected $fillable = [
     'DeptName', 
     'DeptPhysAdd',
     'DeptAddress',
     'DeptTel',
     'DeptEmail',
     'DeptHead',
     'FacultyID'
 ];


   public function faculty(){

      return $this->belongsTo('App\examofficermodels\Faculty');

    }


}
