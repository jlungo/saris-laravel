<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MannerOfEntry extends Model
{
    protected $table = 'mannerofentry';
    protected $fillable = ['mannerofentry'];

}
