<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $fillable = ['regno','bankname','accountnumber','sponsor'];
}
