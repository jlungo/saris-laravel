<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationStatus extends Model
{
    protected $table = 'registrationstatuses';
    protected $fillable = ['status'];
}
