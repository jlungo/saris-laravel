<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnualGpaDatamart extends Model
{
   protected $table='annualgpadatamarts';
   protected $fillable = [
    'regno',
    'Ayear',
    'class',
    'credits',
    'points',
    'gpa',
    'remarks'
   ];

}
