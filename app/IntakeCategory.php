<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntakeCategory extends Model
{
    protected $fillable = ['name','descriptions'];
}
