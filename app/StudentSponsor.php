<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSponsor extends Model
{
  
     protected $fillable =[
            	'regno','address','sponsorId','sponsor_type','name','phone','occupation','email'];
}
