<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependant extends Model
{
    protected $fillable = ['regno','name','gender','relationship','address','phone1','phone2','job'];
}
