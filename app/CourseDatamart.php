<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseDatamart extends Model
{
    protected $table = 'coursedatamarts';
    protected $fillable = [
        'name',
        'regno',
        'Ayear',
        'class',
        'semester',
        'coursecode',
        'coursework',
        'finalexam',
        'total',
        'grade',
        'remarks',
    ];

}
