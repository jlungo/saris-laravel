<?php

namespace App\Exports;

//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SemesterResultExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
   
    private $result;

	public function __construct($result) 
	  {
         $this->result = $result;
	  }


     public function view(): View
    {
    	return view('examofficer.users', [
            'results' => $this->result
        ]);
       
    }

}
