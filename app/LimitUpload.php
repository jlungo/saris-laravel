<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LimitUpload extends Model
{
    protected $table = 'limitupload';
    protected $fillable = ['Ayear','semester','deadline','status'];
}
