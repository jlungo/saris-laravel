<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyLevel extends Model
{
	protected $table = 'studylevels';
    protected $fillable = ['levelname','levelcode'];
}
