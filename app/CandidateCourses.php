<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateCourses extends Model
{
    protected $table = 'candidatecourses';
    protected $fillable = ['Ayear','semester','coursecode','status','regno'];

    
}
