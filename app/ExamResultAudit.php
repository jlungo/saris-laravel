<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamResultAudit extends Model
{
    protected $table = 'examaudit';
    protected $fillable = [
        'Ayear',
        'semester',
        'marker',
        'coursecode',
        'examCAtegory',
        'examdate',
        'examsitting',
        'recorder',
        'recorddate',
        'regno',
        'exaxmno',
        'checked',
        'exaxmscore',
        'exaxmscoreAfter',
        'actiontime',
        'action_user',
        'action_value',
        'status',
        'count',
        'comment'

    ];
}
