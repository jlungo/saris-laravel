<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentContact extends Model
{
    protected $fillable = ['regno','address','phone1','phone2','email1','email2','region','district'];
}
