<?php

namespace App\registrarmodels;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
	
    protected $primaryKey=  'sponsorId';
    
	protected $fillable = [
     'sponsorname', 
     'address',
     'comment'];


}
