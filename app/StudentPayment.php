<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentPayment extends Model
{
    protected $fillable =['regno','payment_type_id','yearofstudy','semester','amount','date_paid'];

    public function payment_type()
    {
    	return $this->belongsTo(PaymentType::class);
    }
}
