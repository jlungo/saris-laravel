<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProgrammeCourse extends Model
{
    protected $table = 'programmecourse';

    protected $fillable = [
        'programme',
        'semester',
        'class',
        'coursecode',
        'intakeyear',
        'status'
    ];
}
