<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassStream extends Model
{
    protected $table = 'classstreams';
    protected $fillable = ['namee','group'];
}
