<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamResult extends Model
{
    protected $table = 'examresults';
    protected $fillable = [
        'Ayear',
        'semester',
        'marker',
        'coursecode',
        'examcategory',
        'coursecode_credit',
        'examdate',
        'examsitting',
        'recorder',
        'recorddate',
        'regno',
        'examno',
        'checked',
        'examscore',
        'status',
        'count',
        'comment'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class,'regno');
    }

}
