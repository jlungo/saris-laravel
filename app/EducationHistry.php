<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationHistry extends Model
{
    protected $fillable = ['regno','level','index_no','start_year','end_year','grade','institution_name'];

}
