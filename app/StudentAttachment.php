<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentAttachment extends Model
{
    protected $fillable = ['attachment_id','attachment_path','applicant_id','filename'];
}

