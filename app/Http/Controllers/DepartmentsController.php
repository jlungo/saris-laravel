<?php

namespace App\Http\Controllers;

use App\examofficermodels\Department;
use App\examofficermodels\Faculty;
use App\examofficermodels\Campus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $departments = Department::all();
        $faculties = Faculty::all();
        return view('examofficer.department.index', ['faculties'=>$faculties,'departments'=>$departments]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        
        $dept = Department::create($request->all());
        return redirect()->route('Department');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $Department = Department::where('DeptID',$id)->first();
        $facultyid = $Department->FacultyID;
        $fac = Faculty::all();
        $Faculty = Faculty::where('FacultyID',$facultyid )->first();
        $campusid = $Faculty->CampusID;
        $Campus = Campus::where('id',$campusid)->first();

        return view('examofficer.department.show',['dept'=>$Department, 'fac'=>$Faculty,'camp'=>$Campus,'faculties'=>$fac]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $data = $request->all();
        $Department = Department::where('DeptID',$id)->first();
        $Department->Update($data);
        return redirect()->route('Department/Details',$Department->DeptID);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $Department = Department::where('DeptID',$id)->first();
         $Department->delete();
        return redirect()->route('Department');
    }
}
