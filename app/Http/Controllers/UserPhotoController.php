<?php

namespace App\Http\Controllers;

use App\UserPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth;

class UserPhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        if (! Gate::allows('registrar')) {
            return abort(404);
        } 

        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'regno' => 'required',
        ]);

        $regno = $request->input('regno');
        $photo_path = $request->file('photo')->store('Students/photo');
        
        $chec = UserPhoto::where('regno',$regno)->first();
        if ($chec) {
                $chec->photo_path = $photo_path;
                $chec->save();

            return redirect()->back()
          ->with('success','Image successfully Changed.');
        }else{

        $photo = new UserPhoto;
        $photo->regno = $regno;
        $photo->photo_path = $photo_path;
        $photo->save();

            return redirect()->back()
          ->with('success','Image successfully uploaded.');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserPhoto  $userPhoto
     * @return \Illuminate\Http\Response
     */
    public function show(UserPhoto $userPhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserPhoto  $userPhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPhoto $userPhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPhoto  $userPhoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPhoto $userPhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPhoto  $userPhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPhoto $userPhoto)
    {
        //
    }
}
