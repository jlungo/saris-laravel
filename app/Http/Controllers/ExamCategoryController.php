<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamCategory;
use Illuminate\Support\Facades\Gate;
use Auth;

class ExamCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ExamCategory::all();

        return view('examofficer.examcategory.index', ['categories'=>$categories]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        ExamCategory::create($request->all());
        return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($examcategory)
    {
       
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{
            $examcategory = ExamCategory::where('id',$examcategory)->first();
            return view('examofficer.examcategory.show', ['instdetails'=>$examcategory]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'description'=>'required|max:100'
        ]);

        $description = $request->input('description');

        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{
            ExamCategory::findOrFail($id)->update(['description'=>$description
            ]);

            return redirect('admin/examcategory');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
