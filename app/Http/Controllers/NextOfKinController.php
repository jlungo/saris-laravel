<?php

namespace App\Http\Controllers;

use App\NextOfKin;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Auth;

class NextOfKinController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $this->validate($request,[
            'name'=>'required|max:100',
            'address'=>'required| max:50',
            'phone1'=>'required',
            'gender'=>'required',
            'relationship'=>'required',
            'regno'=>'required'
        ]);

        $data = $request->all();
        NextOfKin::create($data);

        return redirect()->back()->with('success','Information Added Successfully!');


 }

    /**
     * Display the specified resource.
     *
     * @param  \App\NextOfKin  $nextOfKin
     * @return \Illuminate\Http\Response
     */
    public function show(NextOfKin $nextOfKin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NextOfKin  $nextOfKin
     * @return \Illuminate\Http\Response
     */
    public function edit(NextOfKin $nextOfKin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NextOfKin  $nextOfKin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NextOfKin $nextOfKin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NextOfKin  $nextOfKin
     * @return \Illuminate\Http\Response
     */
    public function destroy($nextOfKin)
    {
            if (!Auth::check()) {
            return redirect()->route('login');
            }
            if (! Gate::allows('registrar')) {
                return abort(404);
            }

            $next = NextOfKin::findOrFail($nextOfKin);
            $next->delete();

            return redirect()->back()->with('success','Information was deleted permanent!');
    }
}
