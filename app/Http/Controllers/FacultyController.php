<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\examofficermodels\Faculty;
use App\examofficermodels\Campus;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FacultyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $campuses = Campus::all();
        $faculties = Faculty::all();
        return view('examofficer.faculty.index', ['faculties'=>$faculties,'campuses'=> $campuses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $faculty = Faculty::create($request->all());
        return redirect()->route('faculty');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function show($FacultyID)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
           $fac= Faculty::where('FacultyID',$FacultyID)->first();
            $campusid = $fac->CampusID;
            $campus = Campus::where('id',$campusid)->first();
            return view('examofficer.faculty.show', ['faculty'=> $fac,'campus'=>$campus]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function edit(Faculty $faculty)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $FacultyID)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $data = $request->all();
        $faculty = Faculty::where('FacultyID',$FacultyID)->first();
        $faculty ->update($data);
        return redirect()->route('Faculty/Details',$faculty->FacultyID);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function destroy($FacultyID)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        
        $faculty = Faculty::where('FacultyID',$FacultyID)->first();
        $faculty->delete();
        return redirect()->route('faculty');
        
    }
}
