<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\examofficermodels\Institution;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Auth;


class InstitutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
       
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar_examofficer')) {
            return abort(404);
        }
        $institution = Institution::all();
        return view('examofficer.institution.index', ['institutions'=>$institution]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $institution = Institution::create($request->all());
        return redirect()->route('institution');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function show(Institution $institution)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
       
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{
            $institution = Institution::where('id',$institution->id)->first();
            return view('examofficer.institution.show', ['instdetails'=>$institution]);
        }

        //   if (! Gate::allows('exam_officer')) {
        //     return abort(404);
        // }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function edit(Institution $institution)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
       if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $data = $request->all();
        $institution = Institution::where('id',$id)->first();
        $institution->update($data);
        return redirect()->route('Institution/Details',$institution->id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $institution = Institution::where('id',$id)->first();
        $institution->delete();
        return redirect()->route('institution');
        }
    }
    
}
