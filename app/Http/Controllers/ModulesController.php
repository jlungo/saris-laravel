<?php

namespace App\Http\Controllers;

use App\examofficermodels\Module;
use App\Http\Controllers\Controller;
use App\StudyLevel;
use App\examofficermodels\Programme;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModulesController extends Controller
{   

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //$this->middleware('auth')->except('index');
       //$this->middleware('auth');
       
    
    }

    public function index()
    {  
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $programme = Programme::all();
        $studylevel = StudyLevel::all();
        $modules = Module::all();
        return view('examofficer.module.index', ['modules'=>$modules,'programmes'=>$programme,'studylevel'=>$studylevel ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
    
        $mod = Module::create($request->all());
        return redirect()->route('Module');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        
        $module = Module::where('moduleid',$id)->first();

        $progid = $module->ProgrammeID;
    
        $programme = Programme::where('ProgrammeID', $progid)->first();
        $progs = Programme::all();
        $studylevel = StudyLevel::all();
        

        return view('examofficer.module.show',['mod'=> $module, 'programme'=>$programme,'programmes'=>$progs,'studylevel'=>$studylevel]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $id = $request->input('module_id');
        $data = $request->all();
        $module = Module::where('moduleid',$id)->first();
         $module->Update($data);
        return redirect()->route('Module/Details', $module->moduleid);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        
        $module = Module::where('moduleid',$id)->first();
        $module ->delete();
        return redirect()->route('Module');
    }
}
