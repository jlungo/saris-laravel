<?php

namespace App\Http\Controllers;

use App\IntakeCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth;

class IntakeCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar_examofficer')) {
            return abort(404);
        }
        $intake = IntakeCategory::where('status','active')->get();

        return view('examofficer.intake.index', ['intakes'=>$intake]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $inputs = IntakeCategory::create($request->all());
        return redirect()->route('intake-categories');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IntakeCategory  $intakeCategory
     * @return \Illuminate\Http\Response
     */
    public function show(IntakeCategory $intake)
    {
        
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{
            $intake = IntakeCategory::where('id',$intake->id)->first();
            return view('examofficer.intake.show', ['intdetails'=>$intake]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IntakeCategory  $intakeCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(IntakeCategory $intakeCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IntakeCategory  $intakeCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $data = $request->all();
        $intake = IntakeCategory::where('id',$id)->first();
        $intake->update($data);
        return redirect()->route('Intake-Category/Details',$intake->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IntakeCategory  $intakeCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $intk = IntakeCategory::where('id',$id)->first();
        $intk->delete();
        return redirect()->route('intake-categories');
        }
    }
}
