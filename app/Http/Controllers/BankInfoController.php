<?php

namespace App\Http\Controllers;

use App\BankInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth;

class BankInfoController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }


        $this->validate($request,[
            'bankname'=>'required|max:30',
            'accountnumber'=>'required|max:30',
            'regno'=>'required'
        ]);

        $data = $request->all();
        BankInfo::create($data);

        return redirect()->back()->with('success','Information Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankInfo  $bankInfo
     * @return \Illuminate\Http\Response
     */
    public function show(BankInfo $bankInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankInfo  $bankInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(BankInfo $bankInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankInfo  $bankInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankInfo $bankInfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankInfo  $bankInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($bankInfo)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
            }
            if (! Gate::allows('registrar')) {
                return abort(404);
            }

            $edu = BankInfo::findOrFail($bankInfo);
            $edu->delete();

            return redirect()->back()->with('success','Information was deleted permanent!');
    }
}
