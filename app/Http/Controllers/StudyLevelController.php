<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\StudyLevel;
use Auth;

class StudyLevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (! Gate::allows('registrar_examofficer')) {
            return abort(404);
        }
        $level = StudyLevel::all();
        return view('examofficer.studylevels.index', ['levels'=>$level]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $levels = StudyLevel::create($request->all());

        return redirect()->route('study level');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($stdylevel)
    {
      
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{
            $level = StudyLevel::findOrFail($stdylevel);

            return view('examofficer.studylevels.show', ['level'=>$level]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
       if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $data = $request->all();
        $level = StudyLevel::findOrFail($id);
        $level->update($data);
        return redirect()->route('studylevel/Details',$level->id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (!(Gate::allows('registrar') || Gate::allows('exam_officer'))) {
            return abort(404);
        }else{

        $level = StudyLevel::where('id',$id)->first();
        $level->delete();
        return redirect()->route('study level');
        }
    }
}
