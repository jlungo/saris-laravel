<?php

namespace App\Http\Controllers;

use App\examofficermodels\ModuleConfiguration;
use App\YearOfStudy;
use App\examofficermodels\Programme;
use App\Semester;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\examofficermodels\Module;
use Auth;

class ModuleConfigurationController extends Controller
{
   public function __construct()
   {
    $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
       
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $modules = ModuleConfiguration::where('status','active')
                                ->where('programme_id',$id)->get();
        $programme = Programme::findOrFail($id)->ProgrammeID;
        $yearofstudy = YearOfStudy::all();
        $semesters = Semester::all();
        $modus = Module::all();

        return view('examofficer.moduleconfigure.index',compact('modules','yearofstudy','programme','semesters','modus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }


       $data = $request->all();

        ModuleConfiguration::create($data);

        return redirect()->back()->with('success','Module Updated');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\examofficermodels\ModuleConfiguration  $moduleConfiguration
     * @return \Illuminate\Http\Response
     */
    public function show(ModuleConfiguration $moduleConfiguration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\examofficermodels\ModuleConfiguration  $moduleConfiguration
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $yearofstudy = YearOfStudy::all();
        $semesters = Semester::all();
        $modus = Module::all();
        $module = ModuleConfiguration::findOrFail($id);

          return view('examofficer.moduleconfigure.show',compact('yearofstudy','semesters','modus','module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\examofficermodels\ModuleConfiguration  $moduleConfiguration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $data = $request->all();
        $modu  = $request->input('id');
        $module = ModuleConfiguration::findOrFail($modu);
        $module->update($data);

        return redirect()->back()->with('success','Module Updated');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\examofficermodels\ModuleConfiguration  $moduleConfiguration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
          if (! Gate::allows('exam_officer')) {
            return abort(404);
            }
        $module = $request->input('module');
        $program = $request->input('program');
        $module = ModuleConfiguration::findOrFail($module);
        $module->delete();

        return redirect('admin/programe/'.$program.'/module-configure')->with('success','Module Configuration was Successfully deleted!');

    }
}
