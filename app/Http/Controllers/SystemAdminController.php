<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class SystemAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    { 
        if (! Gate::allows('system_admin')) {
            return abort(401);
        }
        
        return view('home');
    }

    
    public function dashboard()
    {
       return view('dashboard');
    }

    public function importdata()
    {
       return view('systemadmin.importData');
    }

    public function cleandatabase()
    {
       return view('systemadmin.cleanDatabase');
    }


    public function unknownstudents()
    {
       return view('systemadmin.unknownStudents');
    }


   public function indexstudents()
    {
       return view('systemadmin.indexStudent');
    }

    public function querydb()
    {
       return view('systemadmin.queryDatabase');
    }

    public function connection()
    {
       return view('systemadmin.checkConnections');
    }


    public function checkmessage()
    {
       return view('systemadmin.checkMessage');
    }

     public function loginhistory()
    {
       return view('systemadmin.loginHistory');
    }


}
