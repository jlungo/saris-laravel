<?php

namespace App\Http\Controllers;

use App\PaymentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class PaymentTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $payments = PaymentType::all();

        return view('systemadmin.payments',compact('payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $this->validate($request,[
            'name'=>'required|unique:payment_types',
        ]);

        
       $pay = new PaymentType;
       $pay->name = $request->input('name');
       $pay->descriptions = $request->input('descriptions');
       $pay->save();



         return redirect()->back()->with('success','Payment Updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentType $paymentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function edit($paymentType)
    {
         if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $payment = PaymentType::findOrFail($paymentType);

        return view('systemadmin.editpayment',compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$paymentType)
    {
         if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $this->validate($request,[
           'name'=>'required',
        ]);

       $pay = PaymentType::findOrFail($paymentType);
       $pay->update($request->all());

        return redirect('payments')->with('fail','Payment was Successfully deleted!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function destroy($paymentType)
    {
         if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $pay = PaymentType::findOrFail($paymentType);
        $pay->delete();

        return redirect()->back()->with('success','Payment was Successfully deleted!');
    }
}
