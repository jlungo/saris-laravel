<?php

namespace App\Http\Controllers;

use App\examofficermodels\Programme;
use App\StudyLevel;
use App\examofficermodels\Department;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProgrammesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       //$this->middleware('auth')->except('index');
        $this->middleware('auth');
      
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $programme = Programme::all();
        $studylevel = StudyLevel::all();
        $department = Department::all(); 
        return view('examofficer.programme.index', ['programme'=>$programme,'department' =>$department, 'studylevel'=>$studylevel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $progs = Programme::create($request->all());
        return redirect()->route('Programme');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Programme  $programme
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $programmes = Programme::where('ProgrammeID',$id)->first();
        $studylevel = StudyLevel::all();
        $department = Department::all();
        $Department = Department::where('DeptID', $programmes->DeptID)->first();

        return view('examofficer.programme.show',['programme'=>$programmes, 'dept'=>$Department,'departments'=>$department ,'studylevels'=>$studylevel]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Programme  $programme
     * @return \Illuminate\Http\Response
     */
    public function edit(Programme $programme)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Programme  $programme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $data = $request->all();
        $programmes = Programme::where('ProgrammeID',$id)->first();
        $programmes->Update($data);
        return redirect()->route('Programme/Details',$programmes->ProgrammeID);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Programme  $programme
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
         $programmes = Programme::where('ProgrammeID',$id)->first();
         $programmes->delete();
         return redirect()->route('Programme');

    }
}
