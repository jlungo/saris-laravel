<?php

namespace App\Http\Controllers;

use App\CourseResult;
use Illuminate\Http\Request;

class CourseResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseResult  $courseResult
     * @return \Illuminate\Http\Response
     */
    public function show(CourseResult $courseResult)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseResult  $courseResult
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseResult $courseResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseResult  $courseResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseResult $courseResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseResult  $courseResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseResult $courseResult)
    {
        //
    }
}
