<?php

namespace App\Http\Controllers;

use App\StudentSponsor;
use App\Sponsor;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class StudentSponsorController extends Controller
{
    public function __construct() {
    
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($regno)
    {
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $std = Student::where('id',$regno)->first();
        $sponsor = StudentSponsor::where('regno',$std->regno)->first();
        $sponsors = Sponsor::all();

        return view('student.sponsor',compact('sponsor','regno','std','sponsors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $this->validate($request,[
            'sponsor_type'=>'required',
            'regno'=>'required'
        ]);
        $id = $request->input('regno');
        $student = Student::where('id',$id)->first();

        $spoty = $request->input('sponsor_type');
        if ($spoty=='private') {
              $this->validate($request,[
                    'name'=>'required',
                    'phone'=>'required'
                ]);
             $spon = new StudentSponsor;
             $spon->regno = $student->regno;
             $spon->sponsor_type = 'private';
             $spon->address = $request->input('address');
             $spon->phone = $request->input('phone');
             $spon->name = $request->input('name');
             $spon->occupation = $request->input('occupation');
             $spon->email = $request->input('email');
             $spon->save();

             return redirect()->back()->with('success','Informations Updated!');
        }elseif ($spoty=='institute') {
             $spon = new StudentSponsor;
             $spon->regno = $student->regno;
             $spon->sponsorId = $request->input('sponsorId');
             $spon->sponsor_type = 'institute';
             $spon->save();
             return redirect()->back()->with('success','Informations Updated!');
        }else{
            return redirect()->back()->with('fail','Invalid sponsor type!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentSponsor  $studentSponsor
     * @return \Illuminate\Http\Response
     */
    public function show(StudentSponsor $studentSponsor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentSponsor  $studentSponsor
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentSponsor $studentSponsor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentSponsor  $studentSponsor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'sponsor'=>'required'
        ]);
        $spn = $request->input('sponsor');

        $spons = StudentSponsor::findOrFail($spn);
        $spons->update($request->all());

        return redirect()->back()->with('success','Information Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentSponsor  $studentSponsor
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentSponsor $studentSponsor)
    {
        //
    }
}
