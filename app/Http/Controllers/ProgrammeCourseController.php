<?php

namespace App\Http\Controllers;

use App\examofficermodels\Module;
use App\examofficermodels\Programme;
use Illuminate\Support\Facades\Gate;
use App\ProgrammeCourse;
use App\Semester;
use App\YearOfStudy;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProgrammeCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth')->except('index');
        $this->middleware('auth');
        
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $progcourse = ProgrammeCourse::all();
        $modules = Module::all();
        $programmes = Programme::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all();

        return view('examofficer.registermodule.index',
        [
            'progcourse'=>$progcourse,'modules'=>$modules,'programmes'=>$programmes,
            'semester'=>$semester,'Ayear'=>$Ayear
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        try{
        ProgrammeCourse::create($request->all()); 
        return redirect()->route('Register Module')->with('success','Course registered successfully!');
        }catch(Exception $e){
            return redirect()->route('Register Module')->with('error','Course registration failed possibly already exists');
        }


         
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProgrammeCourse  $programmeCourse
     * @return \Illuminate\Http\Response
     */
    public function show(ProgrammeCourse $programmeCourse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProgrammeCourse  $programmeCourse
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgrammeCourse $programmeCourse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProgrammeCourse  $programmeCourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgrammeCourse $programmeCourse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProgrammeCourse  $programmeCourse
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgrammeCourse $programmeCourse)
    {
        //
    }
}
