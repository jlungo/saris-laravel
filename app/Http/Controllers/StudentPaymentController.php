<?php

namespace App\Http\Controllers;

use App\StudentPayment;
use App\Student;
use App\PaymentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class StudentPaymentController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
         if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $student = Student::findOrFail($id);
        $payments = StudentPayment::where('regno',$student->regno)->get();
        $paytypes = PaymentType::all();

        return view('student.payment',compact('student','payments','paytypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (! Gate::allows('registrar')) {
            return abort(404);
        }

         
          $this->validate($request,[
            'payment_type'=>'required',
            'amount'=>'required',
            'semester'=>'required',
            'student'=>'required'
        ]);
          $std = $request->input('student');
          $student = Student::findOrFail($std);

          $pay = new StudentPayment;
          $pay->amount = $request->input('amount');
          $pay->regno = $student->regno;
          $pay->payment_type_id = $request->input('payment_type');
          $pay->yearofstudy = $student->yearofstudy;
          $pay->semester = $request->input('semester');
          $pay->date_paid = date('Y-m-d');
          $pay->save();

          return redirect()->back()->with('success','Recorded Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentPayment  $studentPayment
     * @return \Illuminate\Http\Response
     */
    public function show(StudentPayment $studentPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentPayment  $studentPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentPayment $studentPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentPayment  $studentPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentPayment $studentPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentPayment  $studentPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy($studentPayment)
    {
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        
        $record = StudentPayment::findOrFail($studentPayment);
        $record->delete();

        return redirect()->back()->with('success','Records Successfully deleted!');
    }
}
