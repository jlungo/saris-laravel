<?php

namespace App\Http\Controllers;

use App\CandidateCourses;
use App\Student;
use App\examofficermodels\Module;
use DB;
use App\Semester;
use App\examofficermodels\Programme;
use App\StudentStatus;
use App\YearOfStudy;
use Illuminate\Http\Request;
use App\MaritalStatus;
use App\ClassStream;
use App\LecturerCourseAllocation;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class StudentController extends Controller
{


    public function __construct() {
    
        $this->middleware('auth');
    }


    public function studentResults(Request $req){

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('student')) {
            return abort(404);
        }

        $ayears = YearOfStudy::all();
        if (Auth::check()) {
            $reg = Auth::user()->username;
        }else{return redirect()->route('login');}
        $ayear = $req->input('ayear');

        if($ayear == ""){
            $ayear =null;
        }
        $ca1 = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['examCAtegory','Coursework'],
            ['semester', 'semester I']
        ];
    
        $se1 = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['examCAtegory','Semester Examination'],
            ['semester', 'semester I']
        ];
    
        $ca2 = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['examCAtegory','Coursework'],
            ['semester', 'semester II']
        ];
    
        $se2 = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['examCAtegory','Semester Examination'],
            ['semester', 'semester II']
        ];
    
        $cz2 = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['semester', 'semester II']
        ];
        $cz1 = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['semester', 'semester I']
        ];
        $cz1se = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['semester', 'semester I'],
            ['examCAtegory','Semester Examination']
        ];
        $cz1ca = [
            ['regno', $reg],
            ['Ayear', $ayear],
            ['semester', 'semester I'],
            ['examCAtegory','Coursework']
        ];
    
    
        //semester 1
    
        $ca1_results = DB::table('examresult')
        ->select('examscore','coursecode')->where($ca1)->groupBy('coursecode','examscore')->get();
    
        $se1_results = DB::table('examresult')
        ->select('examscore','coursecode')->where($se1)->groupBy('coursecode','examscore')->get();
    
        $sem1_coursecode = DB::table('examresult')
        ->select('coursecode')->where($cz1se)->orwhere($cz1ca)->groupBy('coursecode')->get();
    
        $credits1_results = DB::table('examresult')
        ->select('coursecode','coursecode_credit')->where($cz1se)->orwhere($cz1ca)->groupBy('coursecode','coursecode_credit')->get();
    
    
        //semester 2
    
        $ca2_results = DB::table('examresult')
        ->select('examscore','coursecode')->where($ca2)->groupBy('coursecode','examscore')->get();
    
        $se2_results = DB::table('examresult')
        ->select('examscore','coursecode')->where($se2)->groupBy('coursecode','examscore')->get();
    
        $sem2_coursecode = DB::table('examresult')
        ->select('coursecode')->where($cz2)->groupBy('coursecode')->get();
    
        $credits2_results = DB::table('examresult')
        ->select('coursecode','coursecode_credit')->where($cz2)->groupBy('coursecode','coursecode_credit')->get();
    
    
        return view('student.results',[
            'ayears'=>$ayears,
            'Ayear'=>$ayear,'cozcode1'=>$sem1_coursecode ,
            'CA1'=>$ca1_results,'SE1'=>$se1_results,'cozcode2'=>$sem2_coursecode ,
            'CA2'=>$ca2_results,'SE2'=>$se2_results,
            'Crdt1'=>$credits1_results,'Crdt2'=>$credits2_results]);
    
    }


    public function studentCourses(){

        return view('student.courses');
    }

    public function personalinfo(Request $request, $regno)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $this->validate($request,[
            'surname'=>'required',
            'firstname'=>'required',
            'sex'=>'required',
            'dbirth'=>'required',
            'maritalstatus'=>'required',
        ]);

        
       $student = Student::where('id',$regno)->first();
       $fname = $request->input('firstname');
       $mname = $request->input('middlename');
       $lname = $request->input('surname');
       if ($student) {
          $student->update($request->all());

          return redirect('student/'.$student->id)->with('success','Information Updated!');

       }else{

         return redirect()->back()->with('error','Could not find this student!');

     }
       //    $std = new Student;
       //    $std->surname = $lname;
       //    $std->firstname = $fname;
       //    $std->middlename = $mname;
       //    $std->regno = $regno;
       //    $std->sex = $request->input('sex');
       //    $std->maritalstatus = $request->input('maritalstatus');
       //    $std->dbirth = $request->input('dbirth');
       //    if ($std->save()) {
       //        $user = User::where('username',$regno)->first();
       //        if ($user) {
       //            $user->name =  $lname.' '.$fname.' '.$mname;
       //            $user->password = password_hash($lname,PASSWORD_DEFAULT);
       //            $user->save();

       //            return redirect()->back()->with('success','Information recorded Successfully');

       //           }else{
       //               return redirect()->back()->with('error','Operation failed!');
       //           }
                    
       //    }else{
       //          return redirect()->back()->with('error','Operation failed!');
       //    }
       // }

    }

    public function info(Request $request, $regno)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        
        $student = Student::where('id',$regno)->first();

        $student->update($request->all());


       return redirect()->back()->with('success','Information Successfully Updated!');

    }

    public function basicinfo(Request $request, $regno)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $student = Student::where('id',$regno)->first();
        $maritals = MaritalStatus::all();

        return view('student.basicinformation',compact('student','maritals'));
    }


    public function filter(Request $request)
    {
        $progr = $request->input('programe');
        $enty = $request->input('entryear');
        $acad = $request->input('acdamyear');
        if ($progr && $enty && $acad) {
             $students = Student::where('programmeofstudy',$progr)
                             ->where('IntakeValue',$acad)
                             ->where('entryyear',$enty)
                            ->get();
        }elseif($progr && $enty){
             $students = Student::where('programmeofstudy',$progr)
                             ->where('entryyear',$enty)
                            ->get();
        }elseif($progr && $acad){
             $students = Student::where('programmeofstudy',$progr)
                             ->where('IntakeValue',$acad)
                            ->get();
        }elseif ($acad && $enty) {
             $students = Student::where('IntakeValue',$acad)
                             ->where('entryyear',$enty)
                            ->get();
        }elseif ($acad) {
             $students = Student::where('IntakeValue',$acad)
                            ->get();
        }elseif ($enty) {
             $students = Student::where('entryyear',$enty)
                            ->get();
        }elseif ($progr) {
             $students = Student::where('programmeofstudy',$progr)
                            ->get();
        }else{
             $students = array();
        }
        


        return view('registrar.filter',compact('students'));


    }


}
