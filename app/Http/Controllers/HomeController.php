<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
     
        return view('home');
    }


    public function help()
    {
        return view('help');
    }


    public function profile()
    {
        return view('profile');
    }

    public function newsevents()
    {
        $events = Event::orderBy('created_at','DESC')->get();
       //  $users = User::orderBy('name','ASC')->get();

        return view('systemadmin.newsEvents',compact('events'));
    }

}
