<?php

namespace App\Http\Controllers;

use App\examofficermodels\CreditConfiguration;
use App\YearOfStudy;
use App\examofficermodels\Programme;
use App\Semester;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Auth;

class CreditConfigurationController extends Controller
{
    public function __construct()
       {
          $this->middleware('auth');
       }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
         if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $credits = CreditConfiguration::where('status','active')
                                ->where('programme_id',$id)->get();
        $programme = Programme::findOrFail($id)->ProgrammeID;
        $yearofstudy = YearOfStudy::all();
        $semesters = Semester::all();


        return view('examofficer.creditconfigure.index',compact('credits','yearofstudy','programme','semesters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (! Gate::allows('exam_officer')) {
            return abort(404);
        }


        $data = $request->all();

        CreditConfiguration::create($data);

        return redirect()->back()->with('success','Credit Updated');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\examofficermodels\CreditConfiguration  $creditConfiguration
     * @return \Illuminate\Http\Response
     */
    public function show(CreditConfiguration $creditConfiguration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\examofficermodels\CreditConfiguration  $creditConfiguration
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $yearofstudy = YearOfStudy::all();
        $semesters = Semester::all();

        $credit = CreditConfiguration::findOrFail($id);

          return view('examofficer.creditconfigure.show',compact('yearofstudy','semesters','credit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\examofficermodels\CreditConfiguration  $creditConfiguration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
            }

            $data = $request->all();
            $crdt  = $request->input('id');
            $crdt = CreditConfiguration::findOrFail($crdt);
            $crdt->update($data);

            return redirect()->back()->with('success','Module Updated');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\examofficermodels\CreditConfiguration  $creditConfiguration
         * @return \Illuminate\Http\Response
         */
        public function destroy(Request $request)
        {
            if (! Gate::allows('exam_officer')) {
            return abort(404);
            }
            $credit = $request->input('credit');
            $program = $request->input('program');
            $credit = CreditConfiguration::findOrFail($credit);
            $credit->delete();

            return redirect('admin/programe/'.$program.'/credit-configure')->with('success','Credit Configuration was Successfully deleted!');

            }
}
