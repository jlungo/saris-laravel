<?php

namespace App\Http\Controllers;

use App\examofficermodels\Programme;
use App\Role;
use App\YearOfStudy;
use App\MannerOfEntry;
use App\ClassStream;
use App\Student;
use App\examofficermodels\Campus;
use App\MaritalStatus;
use App\StudyLevel;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistrationFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        //$this->middleware('auth')->except('index');
        $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
      
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $ayear = YearOfStudy::all();
        $prog = Programme::all();
        $role = Role::all();
        $entries = MannerOfEntry::all();
        $streams = ClassStream::all();
        $levels = StudyLevel::all();
        $campus = Campus::all();
        $students = Student::all();
        $maritals = MaritalStatus::all();

        return view('registrar.regform.index',
         
         ['ayears'=>$ayear,
         'programme'=>$prog,
         'roles'=>$role, 
         'entries'=>$entries,
         'streams'=>$streams,
         'levels'=>$levels,
         'campus'=>$campus,
         'students'=>$students,
         'maritals'=>$maritals
            ]);
         
    }

 
}
