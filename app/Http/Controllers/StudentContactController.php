<?php

namespace App\Http\Controllers;

use App\StudentContact;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth;

class StudentContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($regno)
    {
        $std = Student::where('id',$regno)->first();
        $contact = StudentContact::where('regno',$std->regno)->first();

        return view('student.contact',compact('contact','regno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $this->validate($request,[
            'region'=>'required|max:40',
            'district'=>'required|max:40',
            'phone1'=>'required|max:20',
            'email1'=>'required|max:40',
            'regno'=>'required'
        ]);
        $id = $request->input('regno');
        $student = Student::where('id',$id)->first();
        $cont = StudentContact::where('regno',$student->regno)->first();
        if ($cont) {
             $cont->address = $request->input('address');
             $cont->district = $request->input('district');
             $cont->region = $request->input('region');
             $cont->phone1 = $request->input('phone1');
             $cont->phone2 = $request->input('phone2');
             $cont->email1 = $request->input('email1');
             $cont->email2 = $request->input('email2');
             $cont->save();

          return redirect('student/'.$id)->with('success','Information Updated Successfully!');
        }else{
             $cont = new StudentContact;
             $cont->regno = $student->regno;
             $cont->address = $request->input('address');
             $cont->district = $request->input('district');
             $cont->region = $request->input('region');
             $cont->phone1 = $request->input('phone1');
             $cont->phone2 = $request->input('phone2');
             $cont->email1 = $request->input('email1');
             $cont->email2 = $request->input('email2');
             $cont->save();

           return redirect('student/'.$id)->with('success','Information Added Successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentContact  $studentContact
     * @return \Illuminate\Http\Response
     */
    public function show(StudentContact $studentContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentContact  $studentContact
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentContact $studentContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentContact  $studentContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentContact $studentContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentContact  $studentContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentContact $studentContact)
    {
        //
    }
}
