<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class ExamOfficerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    { 
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        
        return view('home');
    }


    public function Institution()
    {
        return view('examofficer.institution');
    }

    public function campus()
    {
        return view('examofficer.campus.index');
    }


}
