<?php

namespace App\Http\Controllers;

use App\CandidateCourses;
use App\Student;
use App\examofficermodels\Module;
use App\MannerOfEntry;
use App\Sponsor;
use App\EducationHistry;
use App\NextOfKin;
use App\StudentPayment;
use DB;
use App\Semester;
use App\examofficermodels\Programme;
use App\StudentContact;
use App\UserPhoto;
use App\BankInfo;
use App\MaritalStatus;
use App\StudentStatus;
use App\YearOfStudy;
use Illuminate\Http\Request;
use App\ClassStream;
use App\examofficermodels\Campus;
use App\LecturerCourseAllocation;
use Maatwebsite\Excel\Facades\Excel;
use App\LimitUpload;
use App\RegisteredStudent;
use App\StudentClass;
use App\StudentRestore;
use App\Dependant;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;

class RegistrarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }


    public function index()
    {
        
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
         $maritals = MaritalStatus::all();
        $students = Student::all();
       
        return view('registrar.classlist.index', compact('students','maritals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);

        }

        $this->validate($request,[
            'surname'=>'required',
            'firstname'=>'required',
            'regno'=>'required',
            'dbirth'=>'required',
            'sex'=>'required',
            'maritalstatus'=>'required'
        ]);

        $regno = $request->input('regno');
        //check if regno no is not registered
        $stnt = Student::where('regno',$regno)->first();
        if ($stnt) {
             return redirect()->back()->with('error','Registration number is already taken by someone!, Please re-check properly!');
        }else{
            //save student
            $stdnt  = new Student;
            $stdnt->surname = $request->input('surname');
            $stdnt->firstname = $request->input('firstname');
            $stdnt->middlename = $request->input('middlename');
            $stdnt->dbirth = $request->input('dbirth');
            $stdnt->sex = $request->input('sex');
            $stdnt->regno = $regno;
            $stdnt->maritalstatus = $request->input('maritalstatus');
            $stdnt->save();

            return redirect('student/'.$stdnt->id)->with('success','Student successfully registered!');
           
        }

        //dd("There you are");
        // $email = 'email@email';
        // $password = $request->input('surname');
        // $Mname = $request->input('middlename');
        // $fname = $request->input('firstname');
        // $name = $password.' '.$fname.' '.$Mname;

        // $usr = new User;
        // $usr->username = $username;
        // $usr->name = 'name_of';
        // $usr->email = $username;
        // $usr->password = password_hash($username,PASSWORD_DEFAULT);
        // $usr->role_id = 7;
        // $usr->save();

        // dd($regno);

        // if (Student::create($data)) {
        //     return redirect('student/'.$regno);
        // }else{
            
        // }
        

        // return redirect()->route('Classlists');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($regno)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $student = Student::where('regno',$regno)->first();
        return view('registrar.classlist.show', ['student'=> $student]);
    }

    public function showrestoreLogs(){

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $students = StudentRestore::all();
        return view('registrar.restorelogs.index', ['students'=>$students]);
    }



    public function completedelete(){

        
    }


    public function studentrestore(){

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    public function studentreg(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $modules = Module::all();
        $year = YearOfStudy::all()->sortByDesc("id");
        $semester = Semester::all();
        $class = StudentClass::all();
        $status = StudentStatus::all();

        $prog = Programme::all();
        $reg = $req->input('regno');
        $stud = Student::where('regno',$reg)->first();

        return view('examofficer.studentreg.index', 
        ['student'=>$stud, 'modules'=>$modules, 'programme'=>$prog,
        'semester'=>$semester,'year'=>$year,'class'=>$class,'status'=>$status]);

    }

    public function studentCoursereg(Request $req) {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        if(
            $req->input('coursecode')== ""||
            $req->input('Ayear')== ""||
            $req->input('semester')== ""||
            $req->input('status')== ""||
            $req->input('regno')== ""
            ){
            return back()->with('error', 'Please select all the required information above to register course to a student');
            }
            $course = $req->input('coursecode');
             CandidateCourses::create($req->all());
            return back()->with('success', 'Student was registered successful to '.$course);
    }

    public function studentCourseclear(Request $req) {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        if(
            $req->input('coursecode')== ""||
            $req->input('Ayear')== ""||
            $req->input('semester')== ""||
            $req->input('status')== ""||
            $req->input('regno')== ""
            ){
            return back()->with('error', 'Please select all the required information above to register course to a student');
            }
            $course = $req->input('coursecode');
             CandidateCourses::create($req->all());
            return back()->with('success', 'Student was registered successful to '.$course);
    }


    public function studentremark(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
      
        $year = YearOfStudy::all()->sortByDesc("id");
        $class = StudentClass::all();
        $status = StudentStatus::all();

        $semester = Semester::all();
        $reg = $req->input('regno');
        $stud = Student::where('regno',$reg)->first();
        return view('examofficer.studentremark.index', 
        ['student'=>$stud, 'semester'=>$semester,
        'year'=>$year,'class'=>$class,'status'=>$status]);

    }
    public function updateStudentremark(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $regno = $req->input('regno');
        $semester = $req->input('semester');
        $class = $req->input('class');
        $status = $req->input('status');


    if($semester==''||
          $class==''||
          $status==''){
            return back()->with('error','must select all option to continue');

        }

        $wherevalues = [
            ['regno',$regno],
            ['class' ,$class],
            ['semester',$semester]
        ];

        RegisteredStudent::where($wherevalues)->update(
            ['status'=>$status]
        );        

        return back()->with('success', 'Student status updated succefully to '.$status);

    }



    public function limitUpload(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        } 
        $year   = YearOfStudy::all()->sortByDesc("id");

        $semester = Semester::all();
        $reg = $req->input('regno');
        $stud = Student::where('regno',$reg)->first();
        return view('examofficer.limitupload', 
        ['student'=>$stud, 'semester'=>$semester,'year'=>$year]);

    }

    public function uploadpicture(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        } 

        $req->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'regno' => 'required',
        ]);

        $regno = $req->input('regno');

        $wherevalues = [
            ['regno', $regno]
        ];
        $user = Student::where('regno', $regno)->first();
        $avatarName = $user->regno.'_picture'.time();
        $req->file('avatar')->storeAs('pictures',$avatarName);

        $user->photo = $avatarName;
        $user->where($wherevalues)->update(['photo' => $avatarName]);
        
        return redirect()->route('Student Search')
        ->with('success','You have successfully upload image.');

    }


    public function showuploadpicture(Request $req, $regno)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }   
        return view('registrar.uploadpicture',['regno'=>$regno]);
    }


    public function studentsearch(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $student = null;
        $regno = $req->input('regno');
        $programs = Programme::all();
        $academyear = YearOfStudy::all();

        if($req->input('regno')==""){
            $student = Student::all();
        }else{
            $student = Student::where('regno', $regno)->first();
        }
        return view('registrar.searchstudent',['students' => $student, 'programs'=>$programs, 'academyear'=>$academyear]);
    }

    public function setdeadline(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $ayear = $req->input('Ayear');  
        $status = $req->input('status');  
        $sem = $req->input('semester');  
        $deadline = $req->input('deadline');  
        
        if($ayear=="" || $status==""|| $sem==""|| $deadline==""){
            return back()->with('error', 'Please select all criteria to set Results uploads limit');
        }else{

            try {
                LimitUpload::create($req->all());
                return back()->with('success', 'Successfully set upload limit to the day of '.$deadline);
            
                }catch(Exception $e){
                    try{
             
                        $wherevalues = [
                            ['Ayear', $ayear],
                            ['semester',$sem]
                        ];
                        LimitUpload::where($wherevalues)
                        ->update([
                        'Ayear' => $ayear,
                        'deadline'=>$deadline,
                        'status'=> $status,
                        'semester' => $sem]);
                        return back()->with('success', 'Successfully updated upload limit to the day of '.$deadline); 
                        }catch(Exception $e){
                            return back()->with('error', 'Problem updating the limit upload already set');
                        }
               
            }
            
        }
   
    }


    public function publishexam()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $year = YearOfStudy::all()->sortByDesc("id");
        $semester = Semester::all();
        $programme = Programme::all();

        return view('examofficer.publishexam', 
        ['semester'=>$semester,'year'=>$year,'programme'=>$programme]);
    }


    public function class_list(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $year = YearOfStudy::all()->sortByDesc("id");
        $module = Module::all();

        $coursecode  = $req->input('course');
        $Ayear = $req->input('ayear');


        $c1 = [
            ['coursecode', $coursecode],
        ];

        if($coursecode ==""){
            $studentscourses=null;
        }else{

            $course = DB::table('programmecourse')
            ->select('programme','semester','class')->where($c1)->first();

            $c2 = [
                ['registeredstudents.semester',$course->semester],
                ['students.programmeofstudy',$course->programme],
                // ['registeredstudents.class',  $course->class],
                ['registeredstudents.Ayear', $Ayear],
            ];

            $studentscourses = DB::table('students')
            ->join('registeredstudents', 'registeredstudents.regno', '=', 'students.regno')
            ->select('students.regno','registeredstudents.semester','registeredstudents.class','registeredstudents.Ayear')->where($c2)->get();


            if(!count($studentscourses)>0){
            return redirect()->route('Class Lists')->with('error','No Students list available for '.$coursecode. ' For Academic year '.$Ayear);
            }

        }

        return view('examofficer.classlists', 
        ['year'=>$year,'modules'=>$module,'students'=>$studentscourses,'coursecode'=>$coursecode]);
        
    }




    public function updateClass(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $programme = $req->input('programme');
        $intake = $req->input('inyear');
        $Ayear  = $req->input('year');
        $semester  = $req->input('semester');
        $class = $req->input('class');

        if($programme==''||
        $intake=='' ||
        $Ayear==''||
        $semester==''||
        $class==''){

            return back()->with('error','must select all option to continue');
        }

        try {
            $regnos = Student::select('regno')
            ->where('programmeofstudy', $programme)
            ->where('entryyear', $intake)->get();

            if(!count($regnos)>0){
                return back()->with('error','No Students list to update');
            }
            foreach ($regnos as $regs) {
                    RegisteredStudent::create(
                    [
                        'regno'=>$regs->regno,
                        'Ayear'=>$Ayear,
                        'class' =>$class,
                        'semester'=>$semester,
                        'status'=>'Registered',
                    ]
                );
    
            }
        return redirect()->route('Update Class Lists')->with('success', 'Class list was succefully updated');
        
    }catch(Exception $e){

            return redirect()->route('Update Class Lists')->with('error', 'Class list Already exist');
        }

    }




    public function updateClasslist()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $year = YearOfStudy::all()->sortByDesc("id");
        $programme = Programme::all();
        $semester = Semester::all();
        $class = StudentClass::all();
        
        
        return view('examofficer.updateclasslist', 
        ['programme'=>$programme,'year'=>$year,
        'semester'=>$semester,'class'=>$class]);

    }

    public function course_allocation()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $year = YearOfStudy::all()->sortByDesc("id");
        $module = Module::all();
        $semester = Semester::all();

        $lecture = LecturerCourseAllocation::all();

        return view('examofficer.courseallocation', 
        ['semester'=>$semester,'year'=>$year, 'modules'=>$module,'Lectures'=>$lecture]);

    }

    public function courseallocating(Request $req)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $lecture = LecturerCourseAllocation::create($req->all());
        return redirect()->route('Course Allocation');
    }

    public function courseDeallocating($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $lect = LecturerCourseAllocation::where('id',$id)->first();
        $lect->delete();
        return redirect()->route('Course Allocation');

    }


    public function showImport(){
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $module = Module::all();
        $programme = Programme::all();
        $campuses = Campus::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all()->sortByDesc("id");
        $modules = Module::all();
    
        return view('registrar.showimport',['campuses'=>$campuses, 'module'=> $module, 
        'semester'=>$semester,'Ayear'=>$Ayear,'programme'=>$programme]);

    }
      
    public function import(Request $req) 
      {
        
       
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $programme= $req->input('programme');
        $campus = $req->input('campus');
        $intakeyear = $req->input('year');
        $update = $req->input('update'); 


        if($programme==''|| $campus ==''|| $intakeyear==''){
            return back()->with('error','You must select above details');
        }


        if($req->file('imports_file') ) {
            $path = $req->file('imports_file')->getRealPath();
        } else {
            return back()->with('error','No file selected');
        }
      


        $data = Excel::toArray(null, request()->file('imports_file'));

        if(!empty($data)){
            foreach ($data as $key => $x) {
                unset($x[0]);
                foreach ($x as $key => $value) {
                    $firstname = $value['1'];
                    $surname = $value['3'];
                    $sex = $value['4'];
                    $dob = $value['5'];
                    $formfour = $value['6'];
                    $entrytype = $value['7'];

                    if (($sex || $firstname || $surname || $dob || $formfour || $entrytype)=='') {
                        # skipping this as one of the value is empty

                    }else{
                        //generate new reg NO
                        $regpr = substr($programme, 0, 3);
                        $regyr = substr($intakeyear, 0,4);

                        //Regno format 
                        $newreg = $regpr. "/" .$regyr. "/";

                        //Get our last Student wth Programme and year of entry
                        $last_std = Student::orderBy('id','DESC')->where('programmeofstudy',$programme)->where('yearofstudy',$regyr)->first();

                        //Check if this student exists
                        if ($last_std) {
                            $newreg = substr($last_std->regno, 0, 9);
                            $newreg = $newreg + 1;
                        }else{
                            $newreg = $regpr. "/" .$regyr. "/1";
                        }


                        $stdnt = new Student;
                        $stdnt->regno = $newreg;
                        $stdnt->firstname = $firstname;
                        $stdnt->middlename = $value['2'];
                        $stdnt->surname = $surname;
                        $stdnt->maritalstatus = 'pending';
                        $stdnt->sex = $sex;
                        $stdnt->dbirth = $dob;
                        $stdnt->campus = $campus;
                        $stdnt->yearofstudy = $intakeyear;
                        // $stdnt->sponsor = $value['6'];
                        // $stdnt->formfour = $value['8'];
                        // $stdnt->formsix = $value['10'];
                        // $stdnt->f4year = $value['9'];
                        // $stdnt->f6year = $value['11'];
                        // $stdnt->nationality = $value['13'];
                        // $stdnt->maritalstatus = $value['14'];
                        $stdnt->programmeofstudy = $programme; 
                        $stdnt->save();
                    }
                }

                   // if ($regnumber) {
                        //check if registration no exists
                        // $stdnt = Student::where('regno',$regnumber)->first();
                         // if ($stdnt) {
                         //        $stdnt->firstname = $value['2'];
                         //        $stdnt->middlename = $value['3'];
                         //        $stdnt->surname = $value['4'];
                         //        $stdnt->IntakeValue = $intakeyear;
                         //        $stdnt->sex = $value['5'];
                         //        $stdnt->dbirth = $value['7'];
                         //        $stdnt->campus = $campus;
                         //        // $stdnt->sponsor = $value['6'];
                         //        // $stdnt->formfour = $value['8'];
                         //        // $stdnt->formsix = $value['10'];
                         //        // $stdnt->f4year = $value['9'];
                         //        // $stdnt->f6year = $value['11'];
                         //        $stdnt->nationality = $value['13'];
                         //        $stdnt->maritalstatus = $value['14'];
                         //        $stdnt->programmeofstudy = $programme; 
                         //        $stdnt->update();


                         // }

                         // else{
                                // $stdnt = new Student;
                                // $stdnt->regno = $value['1'];
                                // $stdnt->firstname = $value['2'];
                                // $stdnt->middlename = $value['3'];
                                // $stdnt->surname = $value['4'];
                                // $stdnt->IntakeValue = $intakeyear;
                                // $stdnt->sex = $value['5'];
                                // $stdnt->dbirth = $value['7'];
                                // $stdnt->campus = $campus;
                                // // $stdnt->sponsor = $value['6'];
                                // // $stdnt->formfour = $value['8'];
                                // // $stdnt->formsix = $value['10'];
                                // // $stdnt->f4year = $value['9'];
                                // // $stdnt->f6year = $value['11'];
                                // $stdnt->nationality = $value['13'];
                                // $stdnt->maritalstatus = $value['14'];
                                // $stdnt->programmeofstudy = $programme; 
                                // $stdnt->save();
                         // }
                       // }


                     }
                    
                    
                return redirect()->back()->with('success','Data Imported Successfully');

            }else{
                return back()->with('error', 'No Students to be uploaded!');
            }

    }


    public function register_students(Request $req){

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $programme = $req->input('programme');
        $Inyear = $req->input('Inyear');
        $Ayear = $req->input('Ayear');
        $semester = $req->input('semester');
        $class = $req->input('class');
        $status = $req->input('status');

        if($programme==''||
        $Inyear=='' ||
        $Ayear==''||
        $semester==''||
        $class=='' ||
        $status==''){

            return back()->with('error','must select all option to continue');
        }

        try {
        $regnos = Student::select('regno')
        ->where('programmeofstudy', $programme)
        ->where('entryyear', $Inyear)->get();

            foreach ($regnos as $regs) {
                RegisteredStudent::create(
                [
                    'regno'=>$regs->regno,
                    'Ayear'=>$Ayear,
                    'class' =>$class,
                    'semester'=>$semester,
                    'status'=>$status,
                ]
            );

        }

        }catch(Exception $e){

            return back()->with('error','student were not succefully inserted!');

        }
        return back()->with('success', 
        'Student was registered successful to '.$programme .' as '.$class. ' this academic year of '.$Ayear
    );
    }

    public function prev($regno)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $student = Student::where('id',$regno)->first();
        $regno = $student->regno;

        $entries = MannerOfEntry::all();
        $ayears = YearOfStudy::all();
        $programme = Programme::all();
        $sponsors = Sponsor::all();
        $maritals = MaritalStatus::all();
        $educt = EducationHistry::where('regno',$regno)->get();
        $banks = BankInfo::where('regno',$regno)->get();
        $photo = UserPhoto::where('regno',$regno)->first();
        $contact = StudentContact::where('regno',$regno)->first();
        $dependants = Dependant::where('regno',$regno)->get();
        $nextofs = NextOfKin::where('regno',$regno)->get(); 
        $payments = StudentPayment::where('yearofstudy',$student->yearofstudy)->get();

        return view('registrar.regform.show',compact('student','regno','entries','ayears','programme','sponsors','maritals','educt','banks','photo','contact','dependants','nextofs','payments'));

    }

}