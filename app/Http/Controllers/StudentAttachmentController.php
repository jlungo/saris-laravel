<?php

namespace App\Http\Controllers;

use App\StudentAttachment;
use App\Attachment;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class StudentAttachmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($std)
    {
           if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $attachments = Attachment::all();
        $student = Student::findOrFail($std);
        
        return view('student.attachments.list',compact('attachments','student'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           if (! Gate::allows('registrar')) {
            return abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentAttachment  $studentAttachment
     * @return \Illuminate\Http\Response
     */
    public function show(StudentAttachment $studentAttachment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentAttachment  $studentAttachment
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentAttachment $studentAttachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentAttachment  $studentAttachment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
          if (! Gate::allows('registrar')) {
            return abort(404);
        }
          $this->validate($request,[
            'student'=>'required',
            'attachment'=>'required',
            'file'=>'required'
        ]);

          //check if this attachment is available
          $std = $request->input('student');
          $att = $request->input('attachment');

          $photo_path = $request->file('file')->store('Students/attachments/'.$std);

          $attach = StudentAttachment::where('attachment_id',$att)
                                    ->where('applicant_id',$std)->first();
          //if the attachment exists update the path 
          if ($attach) {
              $attach->attachment_id = $att;
              $attach->applicant_id = $std;
              $attach->attachment_path = $photo_path;
              $attach->save();

           return redirect()->back()->with('success','Successfully Added!');

          }else{
              $attc = new StudentAttachment;
              $attc->attachment_id = $att;
              $attc->applicant_id = $std;
              $attc->attachment_path = $photo_path;
              $attc->save();

          return redirect()->back()->with('success','Successfully Updated!');

      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentAttachment  $studentAttachment
     * @return \Illuminate\Http\Response
     */
    public function destroy($att)
    {
       if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $attachment = StudentAttachment::findOrFail($att);
        $attachment->delete();

         return redirect()->back()->with('success','Attachement  Removed!');
    }
}
