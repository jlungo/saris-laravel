<?php

namespace App\Http\Controllers;

use App\ExamCategory;
use Illuminate\Http\Request;
use App\ExamResult;
use App\Student;
use App\examofficermodels\Campus;
use App\examofficermodels\Module;
use App\YearOfStudy;
use App\Exports\SemesterResultExport;
use App\CourseResult;
use App\Semester;
use DB as db;
use App\CandidateCourses;
use App\CourseDatamart;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use App\examofficermodels\Programme;
use App\LimitUpload;
use App\ProgrammeCourse;
use App\StudyLevel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;
use Barryvdh\DomPDF\Facade as PDF;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Excel;

class GradeBookController extends Controller
{
    public $semstertorecord;
    public $Ayeartorecord;
    public $Inyeartorecord;
    public $programmetorecord;
    public $campustorecord;
    public $moduletorecord;
    public $examcattorecord;
    public $classtorecord;

 public function __construct() {
    $this->middleware('auth');
   
}


public function index(Request $req){
    if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
    $sem = $req->input('semester');
    $year = $req->input('year');
    $prog = $req->input('programme');
    $campus = $req->input('campus');

    $this->semstertorecord = $sem;
    $this->Ayeartorecord =  $year;

    $programme = Programme::all();
    $campuses = Campus::all();
    $semester = Semester::all();
    $Ayear = YearOfStudy::all()->sortByDesc("id");
    $modules = Module::all();
    $examcategory = ExamCategory::all();
  
    if($sem='' || $sem==null || $year='' || $year==null){
        $header = null;
    }else{

        $header = 'Select Appropriate Entries For '.  $this->semstertorecord . ' - '.$this->Ayeartorecord;
    }

    return view('examofficer.gradebook.index',
     ['campuses'=>$campuses, 'programme'=> $programme, 
     'semester'=>$semester,'Ayear'=>$Ayear,'head'=>$header,
     'head'=>$header,'modules'=>$modules,'examcat'=>$examcategory,
     'yea'=>$this->Ayeartorecord,
     'semF'=>$this->semstertorecord ,'progF'=>$prog,'campusF'=>$campus]
    );
}

public function uploadResultsindex(Request $req){ 

    if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }    
    $date = $req->input('date');
    $prog = $req->input('programme');
    $credit = $req->input('credit');
    $category = $req->input('category');
    $course = $req->input('course');
    $semester = $req->input('semester');
    $Ayear  = $req->input('ayear');
    $update = $req->input('overwrite');
    $regnumber = '';
    
    if($req->file('import_file')) {
        $path = $req->file('import_file')->getRealPath();
    } else {
        return back()->with('error','No file selected');
    }

    $data = Excel::toArray(null, request()->file('import_file'));
        if(!empty($data)){
            foreach ($data as $key => $x) {
                unset($x[0]);
                foreach ($x as $key => $value) {
                    $regnumber = $value['0'];
                    $exam_score = $value['1'];
                    if ($regnumber) {
                        //check if registration no exists
                         $stdnt = Student::where('regno',$regnumber)->first();
                            $wherevalues = [
                                        ['regno', $regnumber],
                                        ['examCAtegory',$category],
                                        ['semester', $semester],
                                        ['coursecode', $course],
                                        ['ayear', $Ayear]
                                    ];
                         if ($stdnt) {
                            //check the existance of records
                            $data = ExamResult::where('regno',$regnumber)
                            ->where('examCAtegory', $category)
                            ->where('semester', $semester)
                            ->where('coursecode', $course)
                            ->where('ayear',$Ayear)
                            ->first();
                            if ($data) {
                                Examresult::where($wherevalues)
                                ->update([
                                       'examscore' => $exam_score]);
                            }else{
                                //Data does not exist, save new
                                $data = new Examresult;
                                $data->regno = $regnumber;
                                $data->examcategory = $category;
                                $data->semester = $semester;
                                $data->coursecode = $course;
                                $data->examscore = $exam_score;
                                $data->Ayear = $Ayear;
                                $data->coursecode_credit =rand(5,20);
                                $data->save();
                              }

                              //Initiate variables
                                 $grade=$rk=$point=$sgp=$totalsgp=$unittaken=$unit=$exno=$marks='0';

                              //After updating ExamResults, get sum & update CourseResult
                               $total_score = ExamResult::where('regno',$regnumber)
                                                 ->where('semester', $semester)
                                                 ->where('coursecode', $course)
                                                 ->where('ayear',$Ayear)
                                    ->get();
                            //Marks calculations
                            //Total marks
                                 $marks = $total_score->sum('examscore');
                            //Total Exams
                                 $exno = count($total_score);
                                 if ($exno>1) {
                                     $exno = $exno;
                                 }else{
                                    $exno =1;
                                 }
                            
                            // Average marks
                                 $marks = $marks/$exno;
                                 $marks = number_format($marks,1);

                                 if($marks>=80){
                                    $grade='A';
                                    $remark = 'PASS';
                                    $margin = 80-$marks;
                                    $point=4;
                                    $sgp=$point*$unit;
                                    $totalsgp=$totalsgp+$sgp;
                                    $unittaken=$unittaken+$unit;
                                    if (($margin<=0.5)&&($margin>0)){
                                        $marks=80;
                                    }
                                }elseif($marks>=65){
                                    $grade='B';
                                    $remark = 'PASS';
                                    $margin = 65-$marks;
                                    $point=3;
                                    $sgp=$point*$unit;
                                    $totalsgp=$totalsgp+$sgp;
                                    $unittaken=$unittaken+$unit;
                                    if (($margin<=0.5)&&($margin>0)){
                                        $marks=65;
                                    }
                                }elseif($marks>=50){
                                    $grade='C';
                                    $remark = 'PASS';
                                    $margin = 50-$marks;
                                    $point=2;
                                    $sgp=$point*$unit;
                                    $totalsgp=$totalsgp+$sgp;
                                    $unittaken=$unittaken+$unit;
                                    if (($margin<=0.5)&&($margin>0)){
                                        $marks=50;
                                    }
                                }  //End Marks calculations
                               
                               // Updating results to Course Results
                               $results = CourseResult::where('regno',$regnumber)
                                          ->where('semester', $semester)
                                          ->where('coursecode', $course)
                                          ->where('ayear',$Ayear)->first();
                             //if results exists update if not save new
                                if ($results) {
                                    # Update ... existing
                                   CourseResult::where('regno',$regnumber)
                                          ->where('semester', $semester)
                                          ->where('coursecode', $course)
                                          ->where('ayear',$Ayear)->update([
                                            'grade'=> $grade,
                                            'totalscore'=>$marks,
                                            'remarks'=>$remark,
                                            'points'=>$point
                                          ]);

                                }else{
                                    //save as new
                                    $rslt = new CourseResult;
                                    $rslt->regno = $regnumber;
                                    $rslt->ayear = $Ayear;
                                    $rslt->semester = $semester;
                                    $rslt->coursecode = $course;
                                    $rslt->coursecode_credit = $unittaken;
                                    $rslt->totalscore = $marks;
                                    $rslt->grade = $grade;
                                    $rslt->remarks = $remark;
                                    $rslt->points = $point;
                                    $rslt->save();
                                }
                            }
                          }
                        }
                      }
                    
                    
                return redirect()->back()->with('success','Data Imported Successfully');

            }else{
                return back()->with('error', 'No Students to be uploaded!');
            }


    

}

public function searchResults(Request $req){

    if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
    $reg = $req->input('regno');
    $stud = Student::where('regno',$reg)->first();

    $ca1 = [
        ['regno', $reg],
        ['examCAtegory','Coursework'],
        ['semester', 'semester I']
    ];

    $se1 = [
        ['regno', $reg],
        ['examCAtegory','Semester Examination'],
        ['semester', 'semester I']
    ];

    $ca2 = [
        ['regno', $reg],
        ['examCAtegory','Coursework'],
        ['semester', 'semester II']
    ];

    $se2 = [
        ['regno', $reg],
        ['examCAtegory','Semester Examination'],
        ['semester', 'semester II']
    ];

    $cz2 = [
        ['regno', $reg],
        ['semester', 'semester II']
    ];
    $cz1 = [
        ['regno', $reg],
        ['semester', 'semester I']
    ];
    $cz1se = [
        ['regno', $reg],
        ['semester', 'semester I'],
        ['examCAtegory','Semester Examination']
    ];
    $cz1ca = [
        ['regno', $reg],
        ['semester', 'semester I'],
        ['examCAtegory','Coursework']
    ];


    //semester 1

    $ca1_results = DB::table('examresults')
    ->select('examscore','coursecode')->where($ca1)->groupBy('coursecode','examscore')->get();

    $se1_results = DB::table('examresults')
    ->select('examscore','coursecode')->where($se1)->groupBy('coursecode','examscore')->get();

    $sem1_coursecode = DB::table('examresults')
    ->select('coursecode')->where($cz1se)->orwhere($cz1ca)->groupBy('coursecode')->get();

    $credits1_results = DB::table('examresults')
    ->select('coursecode','coursecode_credit')->where($cz1se)->orwhere($cz1ca)->groupBy('coursecode','coursecode_credit')->get();


    //semester 2
    $ca2_results = DB::table('examresults')
    ->select('examscore','coursecode')->where($ca2)->groupBy('coursecode','examscore')->get();

    $se2_results = DB::table('examresults')
    ->select('examscore','coursecode')->where($se2)->groupBy('coursecode','examscore')->get();

    $sem2_coursecode = DB::table('examresults')
    ->select('coursecode')->where($cz2)->groupBy('coursecode')->get();

    $credits2_results = DB::table('examresults')
    ->select('coursecode','coursecode_credit')->where($cz2)->groupBy('coursecode','coursecode_credit')->get();


    return view('examofficer.resultsearch.index',[
        'student'=>$stud,'cozcode1'=>$sem1_coursecode ,
        'CA1'=>$ca1_results,'SE1'=>$se1_results,'cozcode2'=>$sem2_coursecode ,
        'CA2'=>$ca2_results,'SE2'=>$se2_results,
        'Crdt1'=>$credits1_results,'Crdt2'=>$credits2_results]);

}


public function downloadCourseExcelResults(Request $req)
{
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
    //Capture year and course
     $year = $req->input('year');
     $coursecode = $req->input('course');
    
    if($coursecode=="" || $year==""){
        return back()->with('error','Must select course code to filter results');
    }
    if (!$year=="" && !$coursecode=="") {
        $c= [
            ['coursecode', $coursecode],
            ['Ayear', $year]
        ];

    //Replaced Datamart with CourseResult
    $report = CourseResult::select('regno', 'coursecode', 'coursecode', 'points','totalscore','grade','remarks')->where($c)->get();

        if(count($report)==0){
            return back()->with('error','There is no report for coursecode '.$coursecode.' for academic year '.$year );
        }
        $pdf = PDF::setOptions(['isHtml5ParserEnWled' => true, 'isRemoteEnWled' => true])
         ->loadView('examofficer.courserptpdf', ['report'=>$report, 'cozcode'=>$coursecode]);
            return $pdf->download($coursecode.'_'.$year.'_Reportpdf.pdf');
    }
}


public function downloadSemesterExcelResults(Request $req)
{

      if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

     $this->validate($req,[
        'semester'=>'required',
        'academicyear'=>'required',
        // 'class'=>'required'
     ]);

    $semester = $req->input('semester');
    $intakeyear= $req->input('intakeyear');
    $Ayear= $req->input('academicyear');
    $programme= $req->input('programme');
    $class = $req->input('class');

    if($semester == "" 
    || $programme == ""||$intakeyear == ""
    ||$Ayear == ""|| $class == ""){
        
    }


    $results = CourseResult::where('semester',$semester)
                            ->where('Ayear',$Ayear)->get();

     return Excel::download(new SemesterResultExport($results), 'Semester Results.xlsx');


    // try{

    //     // $data = CourseResult::where('semester',$semester)->get();

    //  return Excel::download(new SemesterResultExport($data), 'Semester Results.xlsx');


        
    // return $Excel->create('Semester Report', function($excel) {

    //     $excel->sheet('New sheet', function($sheet) {
    //         $weacozcodes = [
    //             ['programme', $this->programmetorecord],
    //             ['semester',$this->semstertorecord],
    //             ['class',$this->classtorecord]
    //         ];

    //         $weastudents = [
    //             ['students.programmeofstudy', $this->programmetorecord],
    //             ['students.entryyear',$this->Inyeartorecord],
    //             ['coursedatamarts.class',$this->classtorecord],
    //             ['coursedatamarts.Ayear',$this->Ayeartorecord],
    //             ['coursedatamarts.semester',$this->semstertorecord]
    //         ];

    //         $coursecodes = DB::table('programmecourse')
    //          ->join('modules','programmecourse.coursecode', '=', 'modules.coursecode')
    //          ->select(
    //              'programmecourse.programme',
    //              'programmecourse.coursecode',
    //              'modules.credits')
    //             ->where($weacozcodes)->groupBy(
    //             'programmecourse.programme',
    //             'programmecourse.coursecode',
    //             'modules.credits')->get();

    //         $report = DB::table('students')
    //         ->join('coursedatamarts', 'students.regno', '=', 'coursedatamarts.regno')
    //         ->select(
    //             'coursedatamarts.name',
    //             'students.programmeofstudy',
    //             'students.sex',
    //             'students.regno',
    //             'coursedatamarts.coursecode',
    //             'coursedatamarts.coursework',
    //             'coursedatamarts.finalexam',
    //             'coursedatamarts.total',
    //             'coursedatamarts.grade',
    //             'coursedatamarts.regno'
    //         )->where($weastudents)
    //         ->groupBy(
    //             'coursedatamarts.name',
    //             'students.programmeofstudy',
    //             'students.sex',
    //             'students.regno',
    //             'coursedatamarts.coursecode',
    //             'coursedatamarts.coursework',
    //             'coursedatamarts.finalexam',
    //             'coursedatamarts.total',
    //             'coursedatamarts.grade',
    //             'coursedatamarts.regno'
    //         )->get();

    //         $students = DB::table('students')
    //         ->join('coursedatamarts', 'students.regno', '=', 'coursedatamarts.regno')
    //         ->select('students.firstname', 'students.middlename', 'students.surname', 'students.programmeofstudy', 'students.sex', 'students.regno')
    //         ->where($weastudents)
    //         ->groupBy('firstname', 'middlename', 'surname', 'programmeofstudy', 'sex', 'regno')->get();


            
    //         $cellspanvalue = count($coursecodes)*4+7; 

    //         $cells = ['A','B','C','D','E','F','G','H','I','J','K','L',
    //         'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
    //         'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL',
    //         'AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ'];

    //         $head = 'SCHOOL OF LIBRARY, ARCHIVES AND DOCUMENTATION STUDIES ';
    //         $head4 = "YEAR OF STUDY :  FIRST YEAR\tSEMESTER : SEMESTER I  DATE :......................\tWEIGHT : CA 40% WEIGHT : SE60%";
    //         $head3 = "NTA LEVEL:  4:  FIELD OF STUDY  :  BASIC TECHNICIAN CERTIFICATE IN LIBRARY, RECORDS AND INFORMATION STUDIES";
          
    //         $sheet->mergeCells('A1:'.$cells[$cellspanvalue].'1');
    //         $sheet->mergeCells('A2:'.$cells[$cellspanvalue].'2');
    //         $sheet->mergeCells('A3:'.$cells[$cellspanvalue].'3');
    //         $sheet->mergeCells('A4:'.$cells[$cellspanvalue].'4');
    //         $sheet->mergeCells('A5:'.$cells[$cellspanvalue].'5');
            
    //         $sheet->loadView('examofficer.semesterrptexcel',
    //         ['header'=>$head, 'head3'=>$head3,'head4'=>$head4,'spancount'=>count($coursecodes),
    //          'coursecodes'=>$coursecodes,'reports'=>$report,'students'=>$students]);
    
    //     });

    // }) ->download('xlsx');


    // }catch(Exception $e){
    //     return back()->with('error','Semester results report is unavailable some results are yet to be uploaded');
    // }
   
}



public function downloadAnnualResults(Request $req)
{
    if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

    $programme = $req->input('programme');
    $class = $req->input('class');
    $Inyear = $req->input('Inyear');
  
    $this->programmetorecord=$programme;
    $this->classtorecord = $class;
    $this->Inyeartorecord =$Inyear;

    try {
        $Excel = App::make('excel');
        return $Excel->create('Annual Report', function ($excel) {
            $excel->sheet('New sheet', function ($sheet) {

                $weacozcodes1 = [
                ['programme', $this->programmetorecord],
                ['semester','semester I'],
                ['class',$this->classtorecord]
                ];

                $weacozcodes2 = [
                ['programme', $this->programmetorecord],
                ['semester','semester II'],
                ['class',$this->classtorecord]
                ];

                $weastudentsCA = [
                ['students.programmeofstudy', $this->programmetorecord],
                ['students.entryyear',$this->Inyeartorecord],
                ['students.class',$this->classtorecord],
                ['examresults.semester','semester I'],
                ['examresults.examcategory', 'Coursework']
                ];
                $weastudentsSE = [
                ['students.programmeofstudy', $this->programmetorecord],
                ['students.entryyear',$this->Inyeartorecord],
                ['students.class',$this->classtorecord],
                ['examresults.semester','semester I'],
                ['examresults.examcategory', 'Semester Examination']
                ];

                $weastudentsCA2 = [
                    ['students.programmeofstudy', $this->programmetorecord],
                    ['students.entryyear',$this->Inyeartorecord],
                    ['students.class',$this->classtorecord],
                    ['examresults.semester','semester II'],
                    ['examresults.examcategory', 'Coursework']
                ];

                $weastudentsSE2 = [
                ['students.programmeofstudy', $this->programmetorecord],
                ['students.entryyear',$this->Inyeartorecord],
                ['students.class',$this->classtorecord],
                ['examresults.semester','semester II'],
                ['examresults.examcategory', 'Semester Examination']
                ];

                $weastudents= [
                ['students.programmeofstudy', $this->programmetorecord],
                ['students.entryyear',$this->Inyeartorecord],
                ['students.class',$this->classtorecord]
                ];

                $coursecodes1 = DB::table('programmecourse')
                ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
                ->select(
                    'programmecourse.programme',
                    'programmecourse.coursecode',
                    'modules.credits')
               ->where($weacozcodes1)->groupBy(
               'programmecourse.programme',
               'programmecourse.coursecode',
               'modules.credits')->get();

               $coursecodes2 = DB::table('programmecourse')
               ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
               ->select(
                   'programmecourse.programme',
                   'programmecourse.coursecode',
                   'modules.credits')
              ->where($weacozcodes2)->groupBy(
              'programmecourse.programme',
              'programmecourse.coursecode',
              'modules.credits')->get();

                $students = DB::table('students')
                            ->select('firstname', 'middlename', 'surname', 'programmeofstudy', 'sex', 'regno')->where($weastudents)
                            ->groupBy('firstname', 'middlename', 'surname', 'programmeofstudy', 'sex', 'regno')->get();

                $studentsCA = DB::table('students')
                            ->join('examresults', 'students.regno', '=', 'examresults.regno')
                            ->select(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.coursecode',
                                'examresults.examscore',
                                'examresults.regno'
                            )->where($weastudentsCA)
                            ->groupBy(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.regno',
                                'examresults.coursecode',
                                'examresults.examscore'
                            )->get();
                        
                                $studentsSE = DB::table('students')
                            ->join('examresults', 'students.regno', '=', 'examresults.regno')
                            ->select(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.coursecode',
                                'examresults.examscore',
                                'examresults.regno'
                            )->where($weastudentsSE)
                            ->groupBy(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.regno',
                                'examresults.coursecode',
                                'examresults.examscore'
                            )->get();
                        
                   
                                $studentsCA2 = DB::table('students')
                            ->join('examresults', 'students.regno', '=', 'examresults.regno')
                            ->select(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.coursecode',
                                'examresults.examscore',
                                'examresults.regno'
                            )->where($weastudentsCA2)
                            ->groupBy(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.regno',
                                'examresults.coursecode',
                                'examresults.examscore'
                            )->get();
                        
                                $studentsSE2 = DB::table('students')
                            ->join('examresults', 'students.regno', '=', 'examresults.regno')
                            ->select(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.coursecode',
                                'examresults.examscore',
                                'examresults.regno'
                            )->where($weastudentsSE2)
                            ->groupBy(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.regno',
                                'examresults.coursecode',
                                'examresults.examscore'
                            )->get();
        


            $cellspanvalue = count($coursecodes1)+count($coursecodes2)+6;

            $cells = ['A','B','C','D','E','F','G','H','I','J','K','L',
            'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL',
            'AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ'];


                $head = 'SCHOOL OF LIBRARY, ARCHIVES AND DOCUMENTATION STUDIES';
                $head4 = "YEAR OF STUDY :  FIRST YEAR\tSEMESTER : SEMESTER I  DATE :......................\tWEIGHT : CA 40% WEIGHT : SE60%";
                $head3 = "NTA LEVEL:  4:  FIELD OF STUDY  :  BASIC TECHNICIAN CERTIFICATE IN LIBRARY, RECORDS AND INFORMATION STUDIES";
          
                $sheet->mergeCells('A1:'.$cells[$cellspanvalue].'1');
                $sheet->mergeCells('A2:'.$cells[$cellspanvalue].'2');
                $sheet->mergeCells('A3:'.$cells[$cellspanvalue].'3');
                $sheet->mergeCells('A4:'.$cells[$cellspanvalue].'4');
                $sheet->mergeCells('A5:'.$cells[$cellspanvalue].'5');

            $sheet->loadView('examofficer.annualrptexcel',
            ['header'=>$head, 'head3'=>$head3,'head4'=>$head4,
            'sem1span'=>count($coursecodes1),'sem2span'=>count($coursecodes2),
            'students'=>$students,
            'studentCA2'=>$studentsCA2,'studentSE2'=>$studentsSE2,
            'studentCA'=>$studentsCA,'studentSE'=>$studentsSE,
            'sem1coz'=>$coursecodes1,'sem2coz'=>$coursecodes2]);
            });
        }) ->download('xlsx');
    
    }catch(Exception $e){
    return back()->with('error','Annual results report is unavailable some results are yet to be uploaded');
    }
  
}


public  function downloadtranscriptPdf(Request $req){

 if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

    $regno = $req->input('regno');
    $confirmed = $req->input('confirmed');
    $nta = $req->input('nta');


    try {
        $student = Student::where('regno', $regno)->first();

        // First Year
        // Second Year
        // Third Year
        // Fourth Year



        $weacozcodes1 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester I'],
        ['class','First Year']
    ];

        $weacozcodes2 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester II'],
        ['class','First Year']
    ];

        //1st
        $weacoz1codes1 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester I'],
        ['class','First Year']
    ];

        $weacoz1codes2 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester II'],
        ['class','First Year']
    ];
        //2nd
        $weacoz2codes1 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester I'],
        ['class','Second Year']
    ];

        $weacoz2codes2 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester II'],
        ['class','Second Year']
    ];
        //3rd
        $weacoz3codes1 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester I'],
        ['class','Third Year']
    ];

        $weacoz3codes2 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester II'],
        ['class','Third Year']
    ];

        //4th
        $weacoz4codes1 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester I'],
        ['class','Fourth Year']
    ];

        $weacoz4codes2 = [
        ['programme', $student->programmeofstudy],
        ['semester','semester II'],
        ['class','Fourth Year']
    ];


        // foresults filters



        $cz1se1 = [
        ['programme', $student->programmeofstudy],
        ['examresults.semester','semester I'],
        ['class','First Year'],
        ['examresults.regno', $student->regno],
        ['examresults.Ayear',$student->entryyear],
        ['examresults.examCAtegory','Semester Examination']
    ];
        $cz1ca1 = [
        ['programme', $student->programmeofstudy],
        ['examresults.semester','semester I'],
        ['class','First Year'],
        ['examresults.regno', $student->regno],
        ['examresults.Ayear',$student->entryyear],
        ['examresults.examCAtegory','Coursework']
    ];
        $cz1se2 = [
        ['programme', $student->programmeofstudy],
        ['examresults.semester','semester II'],
        ['class','First Year'],
        ['examresults.regno', $student->regno],
        ['examresults.Ayear',$student->entryyear],
        ['examresults.examCAtegory','Semester Examination']
    ];
        $cz1ca2 = [
        ['programme', $student->programmeofstudy],
        ['examresults.semester','semester II'],
        ['class','First Year'],
        ['examresults.regno', $student->regno],
        ['examresults.Ayear',$student->entryyear],
        ['examresults.examCAtegory','Coursework']
    ];
  

        $codes1 = DB::table('programmecourse')
    ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
    ->select(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits'
    )->where($weacozcodes1)
    ->groupBy(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits'
    )->get();

        $codes2 = DB::table('programmecourse')
    ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
    ->select(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits'
    )->where($weacozcodes2)
    ->groupBy(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits'
    )->get();

        //results
    
        $y1ca1 = DB::table('programmecourse')
    ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
    ->join('examresults', 'examresults.coursecode', '=', 'modules.coursecode')
    ->select(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->where($cz1ca1)
    ->groupBy(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->get();


        $y1se1 = DB::table('programmecourse')
    ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
    ->join('examresults', 'examresults.coursecode', '=', 'modules.coursecode')
    ->select(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->where($cz1se1)
    ->groupBy(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->get();
        ;

        $y1ca2 = DB::table('programmecourse')
    ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
    ->join('examresults', 'examresults.coursecode', '=', 'modules.coursecode')
    ->select(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->where($cz1ca2)
    ->groupBy(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->get();

        $y1se2 = DB::table('programmecourse')
    ->join('modules', 'programmecourse.coursecode', '=', 'modules.coursecode')
    ->join('examresults', 'examresults.coursecode', '=', 'modules.coursecode')
    ->select(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->where($cz1se2)
    ->groupBy(
        'programmecourse.semester',
        'programmecourse.coursecode',
        'modules.modulename',
        'modules.credits',
        'examresults.examscore'
    )->get();

    $pdf = PDF::setOptions(['isHtml5ParserEnWled'=>true, 'isRemoteEnWled' => true])
    ->loadView('examofficer.transcriptpdf',
        ['student'=>$student,'level'=>$nta,'code1yea1'=>$codes1,'code2yea1'=>$codes2,
    'y1ca1'=>$y1ca1,'y1se1'=>$y1se1,'y1ca2'=>$y1ca2,'y1se2'=>$y1se2]);

        return $pdf->download('Transcript.pdf');

    }catch(Exception $e){

        return back()->with('error','Transcript is not ready for this student');

    }

}



public  function downloadStatementPdf(){
    
}

public  function downloadSuppRptPdf(){
    
}

public function importgradebook(Request $req){

    if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

    $semester = $req->input('sem');
    $Ayear = $req->input('year');

    $todate = Carbon::now()->format('Y-m-d');
    $wherevalues = [
        ['Ayear', $Ayear],
        ['semester',$semester]
    ];

    $upload = LimitUpload::where($wherevalues)->first();

    if(!$upload==null){
        if(new DateTime($upload->deadline) < new DateTime($todate) && $upload->status == "set deadline"){
            return back()->with('error','The deadline for uploading results have been set you can no longer Upload');
        }
    }

    $date = $req->input('date');
    if($date==""){
        $date =null;
    }
    $programme = $req->input('prog');
    $campus = $req->input('camp');
    $semester = $req->input('sem');
    $Ayear = $req->input('year');
    $this->Ayeartorecord=$Ayear;
    $course = $req->input('coursecode');
    $category = $req->input('examcat');

    $credit_module = Module::where('coursecode',$course)->first();

    $whereData = [
        ['examcategory',$category],
        ['coursecode', $course],
        ['semester', $semester]
    ];

    $results = DB::table('examresults')->where($whereData)->get();

    return view('examofficer.gradebook.import',
    ['results'=>$results, 'category'=>$category,'course'=>$course, 
    'date'=>$date,
    'programme'=>$programme,
    'campus'=>$campus,
    'ayear'=>$this->Ayeartorecord,
    'semester'=>$semester,'credit'=>$credit_module]);
}


    public function showCourseResult() {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
    $module = Module::all();
    $campuses = Campus::all();
    $semester = Semester::all();
    $Ayear = YearOfStudy::all()->sortByDesc("id");
    $modules = Module::all();


    return view('examofficer.courseresults',['campuses'=>$campuses, 'module'=> $module, 
    'semester'=>$semester,'Ayear'=>$Ayear]);

    }

    public function showSemesterResult() {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $module = Module::all();
        $programme = Programme::all();
        $campuses = Campus::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all()->sortByDesc("id");
        $modules = Module::all();
    
        return view('examofficer.semesterresults',['campuses'=>$campuses, 'module'=> $module, 
        'semester'=>$semester,'Ayear'=>$Ayear,'programme'=>$programme]);

        }

    public function showNtaSemesterResult() {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $module = Module::all();
        $campuses = Campus::all();
        $programme = Programme::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all()->sortByDesc("id");
        $modules = Module::all();

        return view('examofficer.nta_semesterresults',['campuses'=>$campuses, 'module'=> $module, 
        'semester'=>$semester,'Ayear'=>$Ayear,'programme'=>$programme]);
        }

    public function showAnnualResult(Request $req) {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $prog = $req->input('programme');
        if($prog == ""){
            $prog = null;
        }
        $module = Module::all();
        
        $campuses = Campus::all();
        $programme = Programme::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all()->sortByDesc("id");
        $modules = Module::all();

        return view('examofficer.annualresults',['campuses'=>$campuses, 'module'=> $module, 
        'semester'=>$semester,'Ayear'=>$Ayear,'programme'=>$programme,'progs'=>$prog]);
        }


    public function showCandidateTranscript() {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $award = StudyLevel::all();
        $module = Module::all();
        $campuses = Campus::all();
        $programme = Programme::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all()->sortByDesc("id");
        $modules = Module::all();

        return view('examofficer.candidatetranscript',['campuses'=>$campuses, 'module'=> $module, 
        'semester'=>$semester,'Ayear'=>$Ayear,'award'=>$award]);
        }

    public function showCandidateStatement() {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $award = StudyLevel::all();
        $module = Module::all();
        $campuses = Campus::all();
        $programme = Programme::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all()->sortByDesc("id");
        $modules = Module::all();

        return view('examofficer.candidatestatement',['campuses'=>$campuses, 'module'=> $module, 
        'semester'=>$semester,'Ayear'=>$Ayear,'award'=>$award]);
        }


    public function showSupplementaryReport() {
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $module = Module::all();
        $campuses = Campus::all();
        $programme = Programme::all();
        $semester = Semester::all();
        $Ayear = YearOfStudy::all()->sortByDesc("id");
        $modules = Module::all();

        return view('examofficer.supplementaryrpt',['campuses'=>$campuses, 'module'=> $module, 
        'semester'=>$semester,'Ayear'=>$Ayear,'programme'=>$programme]);
        } 



        
}
