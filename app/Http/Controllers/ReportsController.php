<?php

namespace App\Http\Controllers;

use App\AnnualGpaDatamart;
use App\CourseDatamart;
use App\examofficermodels\Module;
use App\examofficermodels\Programme;
use App\Semester;
use App\SemesterGpaDatamart;
use App\StudentClass;
use App\YearOfStudy;
use Exception;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth')->except('index');
        //$this->middleware('auth');
    }
    
    public function showGenerateReport()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $year = YearOfStudy::all()->sortByDesc("id");
        $module = Module::all();
        $semester = Semester::all();
        $programme = Programme::all();
        $class = StudentClass::all();
        
        return view('examofficer.reportgenerate', 
        ['semester'=>$semester,'year'=>$year, 
        'modules'=>$module,'class'=>$class,'programme'=>$programme]);
    }



    public function generate(Request $req){

        
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        
        $Ayear = $req->input('ayear');
        $programme = $req->input('programme');
        $class = $req->input('class');
        $intake = $req->input('inyear');

        //get data results

        $weastudentsCA = [
                ['students.programmeofstudy', $programme],
                ['students.entryyear',$intake],
                ['examresults.examcategory', 'Coursework']
                ];
        $weastudentsSE = [
                ['students.programmeofstudy', $programme],
                ['students.entryyear',$intake],
                ['examresults.examcategory', 'Semester Examination']
                ];


        $studentsCA = DB::table('students')
                            ->join('examresults', 'students.regno', '=', 'examresults.regno')
                            ->select(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.coursecode',
                                'examresults.examscore',
                                'examresults.semester',
                                'examresults.regno'
                            )->where($weastudentsCA)
                            ->groupBy(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.regno',
                                'examresults.coursecode',
                                'examresults.semester',
                                'examresults.examscore'
                            )->get();
                        
        $studentsSE = DB::table('students')
                            ->join('examresults', 'students.regno', '=', 'examresults.regno')
                            ->select(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.coursecode',
                                'examresults.examscore',
                                'examresults.semester',
                                'examresults.regno'
                            )->where($weastudentsSE)
                            ->groupBy(
                                'students.firstname',
                                'students.middlename',
                                'students.surname',
                                'students.programmeofstudy',
                                'students.sex',
                                'students.regno',
                                'examresults.regno',
                                'examresults.coursecode',
                                'examresults.semester',
                                'examresults.examscore'
                            )->get();
                        
        if(!count($studentsSE) > 0){
            return back()->with('error', 'Report couldnot be succefully generated no students selected');
        }                 

        try {
            foreach ($studentsCA as $key => $student) {
                CourseDatamart::create([
                'name'=> $student->surname.' '.$student->surname.' '.$student->surname,
                'regno'=>$student->regno,
                'Ayear'=>$Ayear,
                'class'=>$class,
                'semester'=> $student->semester,
                'coursecode'=>$student->coursecode,
                'coursework'=>$student->examscore,
                'finalexam'=>$studentsSE[$key]->examscore,
                'total'=>$student->examscore+$studentsSE[$key]->examscore,
                'grade'=>$this->grade($student->examscore+$studentsSE[$key]->examscore),
                'remarks'=> $this->points($student->examscore+$studentsSE[$key]->examscore, 0)
            ]);
            }
        }catch(Exception $e){

            return back()->with('error', 'Report couldnot be succefully generated please possibly try again!');
        }

        //generate GPAs
        $Students  = CourseDatamart::select('regno','semester','class','Ayear')
        ->groupBy('regno','semester','class','Ayear')->get();

        foreach($Students as $key => $student){
            $creditz=0; $pointz=0; $gpa = 0;
            $Course = DB::table('coursedatamarts')
            ->join('modules', 'coursedatamarts.coursecode', '=', 'modules.coursecode')
            ->select(
                'modules.credits',
                'coursedatamarts.regno',
                'coursedatamarts.Ayear',
                'coursedatamarts.semester',
                'coursedatamarts.total',
                'coursedatamarts.class'
            )->where('coursedatamarts.semester',$student->semester)->where('coursedatamarts.regno',$student->regno)->groupBy(
            'modules.credits',
            'coursedatamarts.regno',
            'coursedatamarts.Ayear',
            'coursedatamarts.semester',
            'coursedatamarts.total',
            'coursedatamarts.class'
            )->get();
            
            foreach($Course as $course){
                $creditz += $course->credits;
                $pointz += $course->credits * $this->getpoints($course->total);
                $gpa =  substr($pointz/$creditz, 0, 3);
            }

            try {
                SemesterGpaDatamart::create([
                'regno'=>$student->regno,
                'Ayear'=>$student->Ayear,
                'class'=>$student->class,
                'semester'=>$student->semester,
                'credits'=>$creditz,
                'points'=>$pointz,
                'gpa' =>$gpa,
                'remarks'=>$this->gparemark($gpa)


            ]);
            }catch(Exception $e){

                return back()->with('error', 'Failed to generate Semester GPA reports for the students');
            }


        }

        $students = SemesterGpaDatamart::select('regno','class','Ayear')->groupBy('regno','class','Ayear')->get();
        foreach($students as $student){
            $credits =0; $points =0; $gpa =0;
            $results = SemesterGpaDatamart::select('credits','points','gpa','semester')
            ->where('regno', $student->regno)->groupBy('credits','points','gpa','semester')->get();

            foreach($results as $res){
                $credits += $res->credits;
                $points  += $res->points;
                $gpa  += $res->gpa;
            }


            try {
                AnnualGpaDatamart::create([
                'regno'=>$student->regno,
                'Ayear'=>$student->Ayear,
                'class'=>$student->class,
                'credits'=>$credits,
                'points'=>$points,
                'gpa' =>substr($gpa/2, 0, 3),
                'remarks'=>$this->gparemark(substr($gpa/2, 0, 3))
            ]);
    
            }catch(Exception $e){
    
                return back()->with('error', 'Failed to generate Annual GPA reports for the students');
            }



        }

        return back()->with('success', 'Report succefully generated');

    }
       


// prepare datamart to insert
      

public function grade($grade){

    if($grade >=70 && $grade <= 100){
        return 'A';
    }
    if($grade >=65 && $grade < 70){
        return 'B+';
    }
    if($grade >=60 && $grade < 65){
        return 'B';
    }
    if($grade >=40 && $grade < 60){
        return 'C';
    }
    if($grade >=30 && $grade < 40){
        return 'D';
    }
    if($grade < 30){
        return 'E';
    }

} 


public  function points($total, $i)
 {
     if ($total>=40) {
         return 'PASS';
     } else {
         return 'FAIL';
     }
 }

 public  function gparemark($gpa)
 {
     if ($gpa>=2.0) {
         return 'PASS';
     } else {
         return 'FAIL';
     }

 }

public  function getpoints($grade)
{
    if($grade >=70 && $grade <= 100){
        return 5;
    }
    if($grade >=65 && $grade < 70){
        return 4;
    }
    if($grade >=60 && $grade < 65){
        return 3;
    }
    if($grade >=40 && $grade < 60){
        return 2;
    }
    if($grade >=30 && $grade < 40){
        return 1;
    }
    if($grade < 30){
        return 0;
    }
}

} 



