<?php

namespace App\Http\Controllers;

use App\Dependant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth;

class DependantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $this->validate($request,[
            'name'=>'required|max:100',
            'address'=>'required| max:50',
            'phone1'=>'required',
            'gender'=>'required',
            'relationship'=>'required',
            'regno'=>'required'
        ]);

        $data = $request->all();
        Dependant::create($data);

        return redirect()->back()->with('success','Information Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function show(Dependant $dependant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function edit(Dependant $dependant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dependant $dependant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function destroy($dependant)
    {
         if (!Auth::check()) {
            return redirect()->route('login');
            }
            if (! Gate::allows('registrar')) {
                return abort(404);
            }

            $next = Dependant::findOrFail($dependant);
            $next->delete();

            return redirect()->back()->with('success','Information was deleted permanent!');
    }
}
