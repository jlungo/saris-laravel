<?php

namespace App\Http\Controllers;

use App\registrarmodels\Sponsor;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SponsorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //$this->middleware('auth')->except('index');
        $this->middleware('auth');
    }
    
    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $sponsor = Sponsor::all();
        return view('registrar.sponsor.index', ['sponsors'=>$sponsor]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $sponsor = Sponsor::create($request->all());
        return redirect()->route('sponsor');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $sponsor = Sponsor::where('sponsorId',$id)->first();
        return view('registrar.sponsor.show',['sponsor'=>$sponsor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function edit(Sponsor $sponsor)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request,$id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $data = $request->all();
        $sponsor= Sponsor::where('sponsorId',$id)->first();
        $sponsor->update($data);
        return redirect()->route('Sponsor/Details',$sponsor->sponsorId);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }
        $sponsor= Sponsor::where('sponsorId',$id)->first();
        $sponsor->delete();
        return redirect()->route('sponsor');
    }
}
