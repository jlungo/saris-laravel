<?php

namespace App\Http\Controllers;

use App\examofficermodels\Campus;
use App\examofficermodels\Institution;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CampusesController extends Controller
{

  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    { 
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $institution = Institution::all();
        $campuses = Campus::all();
        return view('examofficer.campus.index', ['campuses'=>$campuses, 'institution'=> $institution ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(array $data)
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        //
         $campus = Campus::create($request->all());
         return redirect()->route('campus')->withSuccess('Campus added successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campus  $campus
     * @return \Illuminate\Http\Response
     */
    public function show(Campus $campus)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $campus = Campus::where('id',$campus->id)->first();
        return view('examofficer.campus.show', ['camp'=>$campus]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campus  $campus
     * @return \Illuminate\Http\Response
     */

    public function edit(Campus $campus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campus  $campus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }

        $data = $request->all();
        $campus = Campus::where('id',$id)->first();
        $campus->update($data);
        return redirect()->route('Campus/Details',$campus->id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campus  $campus
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (! Gate::allows('exam_officer')) {
            return abort(404);
        }
        $campus = Campus::where('id',$id)->first();
        $campus->delete();
        return redirect()->route('campus');
    }

}
