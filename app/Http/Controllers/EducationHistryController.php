<?php

namespace App\Http\Controllers;

use App\EducationHistry;
use Illuminate\Http\Request;
use App\Student;
use Illuminate\Support\Facades\Gate;
use Auth;

class EducationHistryController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (! Gate::allows('registrar')) {
            return abort(404);
        }

        $this->validate($request,[
            'level'=>'required|max:30',
            'index_no'=>'required| max:30',
            'start_year'=>'required|max:4',
            'end_year'=>'required|max:4',
            'grade'=>'required',
            'institution_name'=>'required',
            'regno'=>'required'
        ]);



        $id  = $request->input('regno');
        $student = Student::where('id',$id)->first();

        $data = new EducationHistry; 
        $data->level = $request->input('level');
        $data->index_no = $request->input('index_no');
        $data->start_year = $request->input('start_year');
        $data->end_year = $request->input('end_year');
        $data->grade = $request->input('grade');
        $data->institution_name = $request->input('institution_name');
        $data->regno = $student->regno;
        $data->save();

        return redirect()->back()->with('success','Information Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EducationHistry  $educationHistry
     * @return \Illuminate\Http\Response
     */
    public function show(EducationHistry $educationHistry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EducationHistry  $educationHistry
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationHistry $educationHistry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EducationHistry  $educationHistry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationHistry $educationHistry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EducationHistry  $educationHistry
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if (!Auth::check()) {
            return redirect()->route('login');
            }
            if (! Gate::allows('registrar')) {
                return abort(404);
            }

            $edu = EducationHistry::findOrFail($id);
            $edu->delete();

            return redirect()->back()->with('success','Information was deleted permanent!');
    }
}
