<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamMarker extends Model
{
    protected $table = 'exammarker';
    protected $fillable = ['name','address'];
}
