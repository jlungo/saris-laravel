<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SemesterGpaDatamart extends Model
{
    protected $table = 'semestergpadatamarts';
    protected $fillable=[
        'regno',
        'Ayear',
        'class',
        'semester',
        'credits',
        'points',
        'gpa',
        'remarks'
    ]   ;
}
