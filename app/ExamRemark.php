<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamRemark extends Model
{
    protected $table = 'examremark';
    protected $fillable = ['remark','decription','recorder','recorddate'];
}
