<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseResult extends Model
{
    //
   protected $fillable = ['Ayear','semester','coursecode','coursecode_credit','regno','totalscore','remarks','grade','points','gpa'];

   public function student()
   {
   	return $this->belongsTo(Student::class,'regno');
   }
}
