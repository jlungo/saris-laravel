<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LecturerCourseAllocation extends Model
{
    protected $table = 'lectures_course_allocation';
    protected $fillable = ['lecturername','position','semester','coursecode','Ayear'];
}
