<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hostel extends Model
{
    protected $fillable = ['hostelcode', 'name','capacity','address','user_id'];
}
